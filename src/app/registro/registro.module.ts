import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './registro-routing.module';
import { RegistroComponent } from './registro.component';
import {SharedModule} from '../shared/shared.module';
import  {  PdfViewerModule  }  from  'ng2-pdf-viewer';
import { RecaptchaModule, RECAPTCHA_SETTINGS, RecaptchaSettings } from 'ng-recaptcha';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    PdfViewerModule,
    RecaptchaModule
  ],
  exports: [
  ],
  declarations: [
    RegistroComponent,
  ]
})
export class RegistroModule { }
