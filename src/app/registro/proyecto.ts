export class ProyectoEmpresa {
    constructor(
        public activo?: string,
        public idEmpresa?: number,
        public nit?: string,
        public nombre?: string,
        public representanteLegal?: string,

    ) {}
}


export class ProyectoContacto {
    constructor(
        public activo?: string,
        public cargo?: string,
        public celular?: string,
        public email?: string,
        public ext?: string,
        public idContacto?: number,
        public idEmpresa?: number,
        public nombre?: string,
        public telefono?: string

    ) {}
}

export class ProyectoRegistroSolicitudes {
    constructor(
        public idTipoDocumentoCiudadano?: string,
        public numeroDocumentoCiudadano?: string,
        public primerNombreCiudadano?: string,
        public segundoNombreCiudadano?: string,
        public primerApellidoCiudadano?: string,
        public segundoApellidoCiudadano?: string,
        public fechaNacimientoCiudadano?: string,
        public fechaExpedicionCiudadano?: string,

        public emailCiudadano?: string,
        public telefonoFijoCiudadano?: string,
        public idTSexoCiudadano?: string,
        public telefonoCelularCiudadano?: string,
        public aceptarTerminos?: boolean,
        public idParentesco?: string,

    ) {}
}

export class PersonaHogar {
    constructor(
        public idTipoDocumentoCiudadano?: string,
        public numeroDocumentoCiudadano?: string,
        public primerNombreCiudadano?: string,
        public segundoNombreCiudadano?: string,
        public primerApellidoCiudadano?: string,
        public segundoApellidoCiudadano?: string,
        public fechaNacimientoCiudadano?: string,
        public emailCiudadano?: string,
        public idTSexoCiudadano?: string,
        public idParentesco?: string,
    ) {}
}


export class ProyectoPuntoDeVenta {
    constructor(
        public id?: string,
        public idTipoDocumentoCiudadano?: string,
        public numeroDocumentoCiudadano?: string,
        public primerNombreCiudadano?: string,
        public segundoNombreCiudadano?: string,
        public primerApellidoCiudadano?: string,
        public segundoApellidoCiudadano?: string,
        public fechaNacimientoCiudadano?: string,
        public emailCiudadano?: string,
        public telefonoFijoCiudadano?: string,
        public idTSexoCiudadano?: string,
        public telefonoCelularCiudadano?: string,
        public aceptarTerminos?: boolean,
        public idParentesco?: string,





    ) {}
}


export class ProyectoUnidadesDeApoyo {
    constructor(
        public totalCostos?: string,
        public idTipoUniApo?: string,
        public numeroMeses?: string,
        public idEncuestaEmpre?: string,
        public otrosCostosGastos?: string,
        public municipio?: string,
        public direccion?: string,
        public idMunicipio?: string,
        public ingresosObtenidos?: string,
        public idDepartamento?: string,
        public idClase?: string,
        public clase?: string,
        public obtuvoIngresos?: string,
        public idCausal?: string,
        public cuasal?: string,
        public idUnidadApoyo?: string,
        public gastosPersonal?: string,
        public personalOcupado?: string,
        public fechaActualizacion?: string,
        public fechaCreacion?: string,
        public departamento?: string,
        public tipoUniApo?: string,
        public numero?: number,

    ) {}
}







