import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegistroComponent} from './registro.component';

const routes: Routes = [
  {
    path: '',
    component: RegistroComponent,
    data: {
      breadcrumb: 'Registro Solicitud Nueva Encuesta SISBÉN BOGOTÁ'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
