import {Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, EventEmitter, AfterViewInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router';
import { FormularioService } from '../integrador/services/formulario.service';
import { TokenAdminService } from '../integrador/services/token-admin.service';
import { ProyectoContacto, ProyectoEmpresa, ProyectoUnidadesDeApoyo, ProyectoRegistroSolicitudes, PersonaHogar } from './proyecto';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { ConfiguracionService } from '../integrador/services/configuracion.service';
import { LOCALIDADES } from '../integrador/services/localidades-data';
import { BARRIOS } from '../integrador/services/barrio-data';
import { UPZS } from '../integrador/services/upz-data';
import * as moment from 'moment';
import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from 'file-saver';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import { MustMatch, MustMatchEmail } from './must-match.validator';
import { forkJoin, interval } from 'rxjs';
import * as xml2json from 'xml2json';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

import { MatStepper } from '@angular/material/stepper';




declare var require: any
declare var $: any;

const xmlString = `
<Persona xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://schemas.datacontract.org/2004/07/RNEC.DTO">
<AnoResolucion/>
<CodError>0</CodError>
<DepartamentoExpedicion>ATLANTICO</DepartamentoExpedicion>
<EstadoCedula>0</EstadoCedula>
<Estatura i:nil="true"/>
<FechaAfectacion i:nil="true"/>
<FechaDefuncion/>
<FechaExpedicion>30/03/2001</FechaExpedicion>
<FechaNacimiento i:nil="true"/>
<FechaReferencia i:nil="true"/>
<Genero i:nil="true"/>
<GrupoSanguineo i:nil="true"/>
<Informante i:nil="true"/>
<LugarNacimiento i:nil="true"/>
<LugarNovedad i:nil="true"/>
<LugarPreparacion i:nil="true"/>
<MunicipioExpedicion>BARRANQUILLA</MunicipioExpedicion>
<Nuip>72278926</Nuip>
<NumResolucion/>
<Particula/>
<PrimerApellido>GUZMAN</PrimerApellido>
<PrimerNombre>RAFAEL</PrimerNombre>
<SegundoApellido>MARTINEZ</SegundoApellido>
<SegundoNombre>DE JESUS</SegundoNombre>
<Serial i:nil="true"/>
</Persona>
`;

@Component({
  selector: 'app-registro',
  templateUrl:  './registro.component.html',
  styleUrls: [  './registro.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [FormularioService, TokenAdminService, {
    provide: STEPPER_GLOBAL_OPTIONS, useValue: {displayDefaultIndicatorType: false}
  }]
})

export class RegistroComponent implements OnInit {

  dateSent;
  dateReceived;
  todaysdate;
  
  src: string | ArrayBuffer = '../assets/docs/manual.pdf';
  srcDatos = "/assets/docs/politicas.pdf";

  map: any;
  xml: string;



  loading;
  valForm: FormGroup;
  countFile:number;
  isMultiple:boolean;
  listFile:any = [];
  habilitarEnvio: boolean = true;
  hideValidate: boolean = true;
  showErrors: boolean = false;
  habilitarEliminar: boolean = false;
  habilitarEditarPuntos: boolean = false;
  habilitarEditarUnidades: boolean = false;
  respuestaRecaptcha = null;
  habilitarCaptcha = false;
  habiltarValidacion = false;

  conteoIntentos = 0;


  habilitarPaso1 = false;

  PARAMETRO_CUNU = '';
  PARAMETRO_DIRECCION = '';
  PARAMETRO_NUMERO_CONSECUTIVO = '';
  PARAMETRO_LOCALIDAD = '';
  PARAMETRO_BARRIO = '';
  PARAMETRO_ESTRATO = '';

  nombreCalle = [];
  letras = [];
  inmuebles = [];

  referenciaCardinal = [];
  bisReferencia = [];

  direccion = {
    nombreCalle: '',
    numeroPrincipal: '',
    letraPrincipal: '',
    bisPrincipal: '',
    referenciaCardinal: '',
    numeroSecundario: '',
    letraSecundaria: '',
    letraPrincipal2: '',
    letraPrincipal4: '',
    bisSecundario: '',
    numeroCasa: '',
    referenciaCardinal2: '',
    complementoDireccion: '',
    localidad: '',
    barrio: '',
    upz: '',
    inmueble: '',
    numeroInmueble: '',
  };

  direccionCompleta;
  direccionCompletaTranscribir: '';
  todaydate;

  registerForm: FormGroup;
  submitted = false;
  submittedPuntos = false;
  submittedUnidades = false;
  submittedDirecciones = false;
  submittedDireccionesUnidades = false;
  submittedRegistroSolicitudes = false;
  submittedDireccionVivienda = false;

  modulo_contacto: FormGroup;
  modulo_establecimiento: FormGroup;
  modulo_paso_1: FormGroup;
  modulo_paso_2: FormGroup;
  modulo_paso_3: FormGroup;



  modulo_unidades: FormGroup;
  direcciones: FormGroup;
  direccionesUnidades: FormGroup;
  direccionesUnidadesTemp: FormGroup;

  modulo_solicitudes: FormGroup;
  direccionViviendaForm: FormGroup;
  personasHogar: FormGroup;
  habilitarRecibo: boolean = false;

  public listadoErrorGrupo: any = [];



  public localidades: any = [];
  public localidadesFilter: any = [];

  public barrios: any = [];
  public barriosFilter: any = [];

  public upzs: any = [];
  public upzsFilter: any = [];

  contacto = new ProyectoContacto();
  empresa = new ProyectoEmpresa();
  establecimiento = new PersonaHogar();
  unidades = new ProyectoUnidadesDeApoyo();
  modelRegistroSolicitudes = new ProyectoRegistroSolicitudes();

  listadoPuntosDeVentas: any = [];
  listadoUnidadesDeApoyo: any = [];
  listadoPersonasHogar: any = [];
  listadoPersonasHogarRegistraduria: any = [];

  unicoHogarModel: any = [];
  isEditPuntosVentas;
  isEditUnidades;
  isEditContacto;
  isEditFormulario = false;
  isLinear = true;
  isDelete = true;
  usuarios;
  code;

  listadoErroresPuntos: any = [];
  listadoErroresUnidades: any = [];

  displayedColumnsPuntosVentas: string[] = ['idPuntoVenta', 'tipoEstable', 'departamento', 'municipio', 'direccion', 'opciones'];
  dataSourcePuntosVentas: MatTableDataSource<any>;
  @ViewChild('paginatorPuntosVentas', { read: MatPaginator }) paginatorPuntosVentas: MatPaginator;
  @ViewChild('sortPuntosVentas') sortPuntosVentas: MatSort;

  @ViewChild('stepper') private myStepper: MatStepper;
  @ViewChild('htmlData') htmlData:ElementRef;
  @ViewChild('reportContent') reportContent: ElementRef;


  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    public formService: FormularioService,
    public servicio: TokenAdminService,
    public settings: ConfiguracionService,
    private modalService: NgbModal,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig ) {
      this.toastyConfig.theme = 'default';
      this.toastyConfig.position = 'bottom-right';
    }

  ngOnInit() {



     // var json = xml2json.toJson(xmlString);
    //console.log(json, 'PROBANDO CONVERSION' )
    var result;
    var parser = require('xml2js');
    parser.Parser().parseString(xmlString, (e, r) => {result = r});

    // xml to json
    //var json = parser.toJson(xmlString);
    console.log("to json -> %s", JSON.stringify(result));
    //console.log( window.location.origin, 'hrefff', window.location.toString())

 

  this.dateSent = '1900-01-01' 
  this.todaysdate = new Date().getFullYear() + "-" + ("0" + (new Date().getMonth() + 1)).slice(-2) + "-" + ("0" + new Date().getDate()).slice(-2);


  this.modulo_establecimiento = this.formBuilder.group({
    idTipoDocumentoCiudadano: ['', [Validators.required]],
    //numeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(60),Validators.pattern('^[a-zA-Z0-9ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    numeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15),Validators.pattern('^[1-9]+[0-9]*')])],
    confirmNumeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15),Validators.pattern('^[1-9]+[0-9]*')])],

    primerNombreCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    segundoNombreCiudadano: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    primerApellidoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    segundoApellidoCiudadano: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    fechaNacimientoCiudadano: ['', [Validators.required]],

    idTSexoCiudadano: ['', [Validators.required]],
    idParentesco: ['', [Validators.required]],
  }, {
    validator: [MustMatch('numeroDocumentoCiudadano', 'confirmNumeroDocumentoCiudadano')],
  });

  this.modulo_paso_1 = this.formBuilder.group({
    validarPaso1: ['', [Validators.required]]
  });

  this.modulo_paso_2 = this.formBuilder.group({
    validarPaso2: ['', [Validators.required]]
  });

  this.modulo_paso_3 = this.formBuilder.group({
    validarPaso3: ['', [Validators.required]]
  });

  this.direcciones = this.formBuilder.group({
    nombreCalleDireccion: ['', Validators.compose([Validators.required])],
    numeroPrincipalDireccion: ['', Validators.compose([Validators.required, Validators.maxLength(3),Validators.pattern('^[0-9]+$')])],
    letraPrincipalDireccion: ['', Validators.compose([])],
    bisPrincipalDireccion: ['', Validators.compose([])],
    letraPrincipalDireccion2: ['', Validators.compose([])],

    referenciaCardinalDireccion: ['', Validators.compose([])],
    numeroSecundarioDireccion: ['', Validators.compose([Validators.required, Validators.maxLength(3),Validators.pattern('^[0-9]+$')])],
    letraSecundariaDireccion: ['', Validators.compose([])],
    bisSecundarioDireccion: ['', Validators.compose([])],
    numeroCasaDireccion: ['', Validators.compose([Validators.required,Validators.maxLength(3),Validators.pattern('^[0-9]+$')])],
    referenciaCardinal2Direccion: ['', Validators.compose([])],
    letra4Direccion: ['', Validators.compose([])],


    //complementoDireccion: ['', Validators.compose([])],
    complementoDireccion: ['', Validators.compose([Validators.required, Validators.maxLength(200),Validators.pattern('^[a-zA-Z0-9ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],

    localidad: ['', Validators.compose([])],
    barrio: ['', Validators.compose([])],
    upz: ['', Validators.compose([])],
    inmueble: ['', Validators.compose([])],
    numeroInmueble: ['', Validators.compose([Validators.maxLength(3),Validators.pattern('^[0-9]+$')])],



  });

  this.direccionesUnidades = this.formBuilder.group({
    idTipoDocumentoCiudadano: ['', [Validators.required]],
    //numeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(60),Validators.pattern('^[a-zA-Z0-9ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    //numeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15),Validators.pattern('^[1-9]+$')])],
    numeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15),Validators.pattern('^[1-9]+[0-9]*')])],
    confirmNumeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15),Validators.pattern('^[1-9]+[0-9]*')])],


    primerNombreCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    segundoNombreCiudadano: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    primerApellidoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    segundoApellidoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    fechaNacimientoCiudadano: ['', [Validators.required]],
    fechaExpedicionCiudadano: ['', [Validators.required]],

    emailCiudadano: ['', Validators.compose([ Validators.required, Validators.maxLength(50),Validators.pattern('^[a-zA-Z0-9]+([.-_]?[a-zA-Z0-9]+[.-_]?)*@[a-zA-Z0-9.-_]+\\.[a-zA-Z]{2,4}$')])],
    confirmEmailCiudadano: ['', Validators.compose([ Validators.required, Validators.maxLength(50),Validators.pattern('^[a-zA-Z0-9]+([.-_]?[a-zA-Z0-9]+[.-_]?)*@[a-zA-Z0-9.-_]+\\.[a-zA-Z]{2,4}$')])],

    idTSexoCiudadano: ['', [Validators.required]],
    telefonoFijoCiudadano: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10),Validators.pattern('^[0-9]+$')])],
    telefonoCelularCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10),Validators.pattern('^[0-9]+$')])],
    terminosYCondiciones: ["", [Validators.required]],
    unicoHogar: ["", [Validators.required]],
    idParentesco: ['', [Validators.required]],
    captcha: ["", [Validators.required]]
  }, {
    validator: [MustMatch('numeroDocumentoCiudadano', 'confirmNumeroDocumentoCiudadano'), MustMatchEmail('emailCiudadano', 'confirmEmailCiudadano')],
  });


  this.direccionesUnidadesTemp = this.formBuilder.group({
    idTipoDocumentoCiudadano: ['', [Validators.required]],
    //numeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(60),Validators.pattern('^[a-zA-Z0-9ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    //numeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15),Validators.pattern('^[1-9]+$')])],
    numeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15),Validators.pattern('^[1-9]+[0-9]*')])],
    confirmNumeroDocumentoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(15),Validators.pattern('^[1-9]+[0-9]*')])],


    primerNombreCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    segundoNombreCiudadano: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    primerApellidoCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    segundoApellidoCiudadano: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(50),Validators.pattern('^[a-zA-ZáéíóúýÁÉÍÓÚÝñÑïöüÿÄËÏÖÜŸ ]*$')])],
    fechaNacimientoCiudadano: ['', [Validators.required]],
    fechaExpedicionCiudadano: ['', [Validators.required]],

    emailCiudadano: ['', Validators.compose([ Validators.required, Validators.maxLength(50),Validators.pattern('^[a-zA-Z0-9]+([.-_]?[a-zA-Z0-9]+[.-_]?)*@[a-zA-Z0-9.-_]+\\.[a-zA-Z]{2,4}$')])],
    confirmEmailCiudadano: ['', Validators.compose([ Validators.required, Validators.maxLength(50),Validators.pattern('^[a-zA-Z0-9]+([.-_]?[a-zA-Z0-9]+[.-_]?)*@[a-zA-Z0-9.-_]+\\.[a-zA-Z]{2,4}$')])],

    idTSexoCiudadano: ['', [Validators.required]],
    telefonoFijoCiudadano: ['', Validators.compose([Validators.minLength(10), Validators.maxLength(10),Validators.pattern('^[0-9]+$')])],
    telefonoCelularCiudadano: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10),Validators.pattern('^[0-9]+$')])],
    terminosYCondiciones: ["", [Validators.required]],
    unicoHogar: ["", [Validators.required]],
    idParentesco: ['', [Validators.required]],
    captcha: ["", [Validators.required]]
  }, {
    validator: [MustMatch('numeroDocumentoCiudadano', 'confirmNumeroDocumentoCiudadano'), MustMatchEmail('emailCiudadano', 'confirmEmailCiudadano')],
  });


  this.direccionViviendaForm = this.formBuilder.group({
    direccionVivienda: ['', [Validators.required]],
    direccionViviendaTranscribir: ['', Validators.compose([])],
  });

  this.personasHogar = this.formBuilder.group({
    listadoPersonas: [[], [Validators.required]],
  });

  this.direccionViviendaForm.get('direccionVivienda').enable();

  this.nuevoRegistroSolicitud();

  this.loading = true;    

    $( () => {$('[data-toggle="tooltip"]').tooltip();});

  this.nombreCalle = [
      { id: 0, nombre: 'Av Cll.', value: 'AC' },
      { id: 1, nombre: 'Av Cr.', value: 'AK' },
      { id: 2, nombre: 'CL', value: 'CL'},
      { id: 3, nombre: 'Dg', value: 'DG' },
      { id: 4, nombre: 'Kr', value: 'KR' },
      { id: 5, nombre: 'Tr', value: 'TV'},
      { id: 6, nombre: 'Vía', value: 'VIA'},
    ];  

    this.inmuebles = [
      { id: 0, nombre: 'Apartamento', value: 'APTO' },
      { id: 1, nombre: 'Interior', value: 'INT' },
      { id: 2, nombre: 'Manzana', value: 'MZ'},
      { id: 3, nombre: 'Casa', value: 'CS' },
      { id: 4, nombre: 'Albergue', value: 'AL' },
      { id: 5, nombre: 'Unidad', value: 'UN'},
    ];  

  this.letras = [
      { id: 0, nombre: 'A' },
      { id: 1, nombre: 'B' },
      { id: 2, nombre: 'C' },
      { id: 3, nombre: 'D' },
      { id: 4, nombre: 'E' },
      { id: 5, nombre: 'F'},
      { id: 6, nombre: 'G'},
      { id: 7, nombre: 'H'},
      { id: 8, nombre: 'I'},
      { id: 9, nombre: 'J'},
      { id: 10, nombre: 'K'},
      { id: 11, nombre: 'L'},
      { id: 12, nombre: 'M'},
      { id: 13, nombre: 'N'},
      { id: 14, nombre: 'O'},
      { id: 15, nombre: 'P'},
      { id: 16, nombre: 'Q'},
      { id: 17, nombre: 'R'},
      { id: 18, nombre: 'S'},
      { id: 19, nombre: 'T'},
      { id: 20, nombre: 'U'},
      { id: 21, nombre: 'V'},
      { id: 22, nombre: 'W'},
      { id: 23, nombre: 'X'},
      { id: 24, nombre: 'Y'},
      { id: 25, nombre: 'Z'},
    ];  

    this.referenciaCardinal = [
      { id: 0, nombre: 'Este', value: 'E' },
      { id: 1, nombre: 'Sur', value: 'S' },
    ];

    this.bisReferencia= [
      { id: 0, nombre: 'BIS', value: 'BIS' },
    ];

    this.nuevoCargue()
    this.cargarFormulario();
    $("#modalImagenPortada").modal("show");

    this.usuarios = [
      {
        "id": 1,
        "name": "Leanne Graham",
        "email": "sincere@april.biz",
        "phone": "1-770-736-8031 x56442"
      },
      {
        "id": 2,
        "name": "Ervin Howell",
        "email": "shanna@melissa.tv",
        "phone": "010-692-6593 x09125"
      },
      {
        "id": 3,
        "name": "Clementine Bauch",
        "email": "nathan@yesenia.net",
        "phone": "1-463-123-4447",
      },
      {
        "id": 4,
        "name": "Patricia Lebsack",
        "email": "julianne@kory.org",
        "phone": "493-170-9623 x156"
      },
      {
        "id": 5,
        "name": "Chelsey Dietrich",
        "email": "lucio@annie.ca",
        "phone": "(254)954-1289"
      },
      {
        "id": 6,
        "name": "Mrs. Dennis",
        "email": "karley@jasper.info",
        "phone": "1-477-935-8478 x6430"
      }
    ];

  //this.descarga()
  this.conteoIntentos = 0;


  }

  ngAfterViewInit() {
    // ...
    this.createCaptcha();

  }

  get f() { return this.registerForm.controls; }
  get puntosVentasErrors() { return this.modulo_establecimiento.controls; }
  get direccionesErrors() { return this.direcciones.controls; }
  get direccionesErrorsUnidades() { return this.direccionesUnidades.controls; }
  get direccionViviendaErrors() { return this.direccionViviendaForm.controls; }
  get personasHogarErrors() { return this.personasHogar.controls; }


  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true,  }).result.then((result) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  cargarFormulario() {
    this.localidades = LOCALIDADES;
    this.barrios = BARRIOS;
    this.upzs = UPZS;
  }

  nuevoCargue(){
    this.paginatorPuntosVentas._intl.itemsPerPageLabel = 'Registros por pagina:';
    this.paginatorPuntosVentas._intl.firstPageLabel = 'Primera pagina';
    this.paginatorPuntosVentas._intl.previousPageLabel = 'Pagina anterior';
    this.paginatorPuntosVentas._intl.nextPageLabel = 'Pagina siguiente';
    this.paginatorPuntosVentas._intl.lastPageLabel = 'Ultima pagina';
    this.dataSourcePuntosVentas = new MatTableDataSource(this.listadoPersonasHogar);
    this.dataSourcePuntosVentas.paginator = this.paginatorPuntosVentas;
    this.dataSourcePuntosVentas.sort = this.sortPuntosVentas;
    this.loading = false;  
  }


  getHogarUnipersonal(event) {
    if (event != undefined && event!= null && event != '') {
      if (event === '1'){

        this.personasHogar.get('listadoPersonas').setValidators([]);
        this.personasHogar.get('listadoPersonas').disable();
        this.personasHogar.updateValueAndValidity();
        this.personasHogar.get('listadoPersonas').setValidators([]);
        this.personasHogar.get('listadoPersonas').disable();
        this.personasHogar.updateValueAndValidity();
        this.listadoPersonasHogar = [];
        this.paginatorPuntosVentas._intl.itemsPerPageLabel = 'Registros por pagina:';
        this.paginatorPuntosVentas._intl.firstPageLabel = 'Primera pagina';
        this.paginatorPuntosVentas._intl.previousPageLabel = 'Pagina anterior';
        this.paginatorPuntosVentas._intl.nextPageLabel = 'Pagina siguiente';
        this.paginatorPuntosVentas._intl.lastPageLabel = 'Ultima pagina';
        this.dataSourcePuntosVentas = new MatTableDataSource(this.listadoPersonasHogar);
        this.personasHogar.get('listadoPersonas').setValue(this.listadoPersonasHogar)

      } else {
        
        this.personasHogar.get('listadoPersonas').setValidators([Validators.required]);
        this.personasHogar.get('listadoPersonas').enable();
        this.personasHogar.updateValueAndValidity();
        this.personasHogar.get('listadoPersonas').setValidators([Validators.required]);
        this.personasHogar.get('listadoPersonas').enable();
        this.personasHogar.updateValueAndValidity();
      }
    } else {

      this.personasHogar.get('listadoPersonas').setValidators([Validators.required]);
      this.personasHogar.get('listadoPersonas').enable();
      this.personasHogar.updateValueAndValidity();
    }
  }


  getFilterBarriosUpz(payload) {
    if (payload != null && payload != undefined && payload != '' ) {
      this.direccion.barrio = '';
      this.direcciones.get('barrio').setValue('')
      this.direccion.upz = '';
      this.direcciones.get('upz').setValue('')
      this.barriosFilter = this.barrios.filter( (item) => Number(item.CODIGO_LOCALIDAD) === Number(payload));
      this.upzsFilter = this.upzs.filter( (item) => Number(item.CODIGO_LOCALIDAD) === Number(payload));
    } else  {
      this.direccion.barrio = '';
      this.direcciones.get('barrio').setValue('')
      this.direccion.upz = '';
      this.direcciones.get('upz').setValue('')
      this.barriosFilter = [];
      this.upzsFilter = [];
    }
  }


  nuevoRegistroSolicitud(){
    this.modelRegistroSolicitudes = {}
    this.modelRegistroSolicitudes.idTipoDocumentoCiudadano = '1';
    this.modelRegistroSolicitudes.idTSexoCiudadano = '';
    this.modelRegistroSolicitudes.idParentesco = '1';
    this.submittedRegistroSolicitudes = false;
    this.loading = false;

  }

  validacionInicial(){


    if (this.modelRegistroSolicitudes.numeroDocumentoCiudadano != null && this.modelRegistroSolicitudes.primerApellidoCiudadano != null && this.modelRegistroSolicitudes.segundoApellidoCiudadano != null && this.modelRegistroSolicitudes.primerNombreCiudadano != null  && this.modelRegistroSolicitudes.fechaExpedicionCiudadano != null && this.modelRegistroSolicitudes.aceptarTerminos != null){

      this.formService.consultarDocumento(this.modelRegistroSolicitudes.numeroDocumentoCiudadano).subscribe((data: {}) => {
  
        let dataReceive: any = data;
  
  
        if (this.modelRegistroSolicitudes.numeroDocumentoCiudadano == dataReceive.Nuip ){

          if (dataReceive.PrimerApellido.toUpperCase() === this.modelRegistroSolicitudes.primerApellidoCiudadano.toUpperCase() &&  
          dataReceive.SegundoApellido.toUpperCase() === this.modelRegistroSolicitudes.segundoApellidoCiudadano.toUpperCase() && 
          dataReceive.PrimerNombre.toUpperCase() === this.modelRegistroSolicitudes.primerNombreCiudadano.toUpperCase() && 
          dataReceive.FechaExpedicion ===  moment(this.modelRegistroSolicitudes.fechaExpedicionCiudadano).format('DD/MM/YYYY') ){
  
            this.habiltarValidacion = true;
  
        } else {
  
          const toastOptions: ToastOptions = {
            title: 'Validación Fallida',
            msg: `No se ha podido validar la información del solicitante. Por favor registre su número de documento, nombres, apellidos y fecha de expedición conforme a su documento de identidad.`,
            showClose: true,
            timeout: 8000,
            theme: 'bootstrap'
        };
          this.toastyService.error(toastOptions);
        }
        } else {
          Swal.fire(
            '¡Atención!',
            'Algunos de los datos diligenciados no son correctos, verifiquelos ó dirijase a un punto de atención CADE ó SUPERCADE',
            'warning'
          );
        }


  
      }); 
    } else {
      const toastOptions: ToastOptions = {
        title: 'Faltan campos por diligenciar',
        msg: `Debe diligenciar los campos requeridos.`,
        showClose: true,
        timeout: 5000,
        theme: 'bootstrap'
    };
      this.toastyService.error(toastOptions);

    }



  }


  cancelarAutorizacionDatos() {
    $("#modalAutorizacionDatos").modal("hide");
  }

  cancelarImagenPortada() {
    $("#modalImagenPortada").modal("hide");
  }

  aceptarAutorizacionDatos() {
    //$("#modalAutorizacionDatos").modal("hide");
    this.modalService.dismissAll();
    this.modelRegistroSolicitudes.aceptarTerminos = true;
  }

  cancelarManual() {
    $("#modalManual").modal("hide");
    this.refresh()
  }



  cancelarReporteGo() {
    $("#modalReporte").modal("hide");
    this.refresh();
  }


  aceptarManual() {
    $("#modalManual").modal("hide");
  }

  cancelarDireccion() {
    $('#modalDireccion').modal('hide');
    this.submittedDirecciones = false;
  }

  cancelarPanelError() {
    $('#modalErrores').modal('hide');
  }

  cancelarHogar() {
    $('#modalHogar').modal('hide');
    this.nuevoIntegranteHogar('new');
    this.submittedPuntos = false;
  }


  guardarIntegranteHogar() {
    this.submittedPuntos = true;
    if (this.modulo_establecimiento.invalid) {
        return;
    }

    $('#modalHogar').modal('hide')

    let sizeHogar = this.listadoPersonasHogar.length 

    if(sizeHogar <= 23) {

      let dataEdad: any = ''

      dataEdad = this.calculaEdad(this.establecimiento.fechaNacimientoCiudadano)

          if (dataEdad === 'old' && (this.establecimiento.idTipoDocumentoCiudadano === '1' || this.establecimiento.idTipoDocumentoCiudadano === '3' || this.establecimiento.idTipoDocumentoCiudadano === '8' || this.establecimiento.idTipoDocumentoCiudadano === '6' || this.establecimiento.idTipoDocumentoCiudadano === '6')){

            this.grabarHogarArray()

          } else if (dataEdad === 'young' && (this.establecimiento.idTipoDocumentoCiudadano === '2' || this.establecimiento.idTipoDocumentoCiudadano === '3' || this.establecimiento.idTipoDocumentoCiudadano === '6')){

            this.grabarHogarArray()

          } else if (dataEdad === 'child' && (this.establecimiento.idTipoDocumentoCiudadano === '4' || this.establecimiento.idTipoDocumentoCiudadano === '5' || this.establecimiento.idTipoDocumentoCiudadano === '6')){

            this.grabarHogarArray()

          } else {


            Swal.fire({
              title: '¡Atención!',
              icon: 'warning',
              html:
                `El tipo de documento no es coherente con la edad. Por favor verifique la información digitada. 
                <br>
                *Validar: tipo de documento por rangos de edad: <br>
                <ul>
                <li>Mayores de 18 años (CC, PEP, Salvoconducto, CE)</li>
                <li>Entre 7 y 17 años (TI, PEP, Salvoconducto, CE)</li>
                <li>Menores de 7 años (RC, DNI, Pasaporte, PEP, Salvoconducto)</li>
              </ul>`,
              showCloseButton: false,
              showCancelButton: false,
              focusConfirm: true,
              confirmButtonText:
                ' Aceptar',
            })

              this.nuevoIntegranteHogar('');
              this.submittedPuntos = false;
          }

    } else {
      Swal.fire(
        '¡Atención!',
        'Para cada hogar, sólo es posible registrar un máximo de 25 personas.',
        'warning'
      );
      this.nuevoIntegranteHogar('');
      this.submittedPuntos = false;

    }
  }

  grabarHogarArray(){
    let conyuge = 0;
    let padres = 0;
    for (let i = 0; i < this.listadoPersonasHogar.length; i++) {
      if (this.listadoPersonasHogar[i].idParentesco === '2'){
        conyuge = conyuge + 1
      }
    }

    for (let i = 0; i < this.listadoPersonasHogar.length; i++) {
      if (this.listadoPersonasHogar[i].idParentesco === '5'){

        padres = padres + 1
      }
    }

    for (let i = 0; i < this.listadoPersonasHogar.length; i++) {
      if (this.listadoPersonasHogar[i].numeroDocumentoCiudadano === this.establecimiento.numeroDocumentoCiudadano){

        Swal.fire(
          '¡Atención!',
          'El integrante que quiere ingresar ya se encuentra en la lista.',
          'warning'
        );
        this.resetGrupoHogar();
        this.submittedPuntos = false;
        return      }
    }


    if (this.establecimiento.idParentesco === '2' && conyuge === 1){
      Swal.fire(
        '¡Atención!',
        'En el registro de la solicitud de encuesta, solo puede existir un(a) esposo(a) o compañero(a) del jefe del hogar.',
        'warning'
      );
      this.resetGrupoHogar();
      this.submittedPuntos = false;
      return
    }

    if (this.establecimiento.idParentesco === '5' && padres === 2){
      Swal.fire(
        '¡Atención!',
        'En el registro de la solicitud de encuesta, solo puede agregar un Padre/Padrastro y Una Madre/Madrastra del jefe del hogar.',
        'warning'
      );
      this.resetGrupoHogar();
      this.submittedPuntos = false;
      return
    }
     
    this.listadoPersonasHogar.push(this.establecimiento);
    this.dataSourcePuntosVentas = new MatTableDataSource(this.listadoPersonasHogar);
    this.personasHogar.get('listadoPersonas').setValue(this.listadoPersonasHogar)
    this.nuevoIntegranteHogar('');
    this.submittedPuntos = false;
  }


  listaDatosHogar() {
    if(this.unicoHogarModel != '1'){
      if (this.personasHogar.invalid) {
        Swal.fire(
          '¡Atención!',
          'Debe agregar los integrantes que componen el hogar.',
          'warning'
        );
          return;
      } else {

        this.settings.showLoadingRequest(true);

        let listaPersonasAnalizar: any = [];

        for (let i = 0; i < this.listadoPersonasHogar.length; i++) {
          listaPersonasAnalizar.push(
          {
            "PARAM_DOCUMENTO_PERSONA" : this.listadoPersonasHogar[i].numeroDocumentoCiudadano, 
            "PARAM_CALLE" : '',
            "PARAM_NUMERO1" : '',
            "PARAM_LETRA1" : '',
            "PARAM_BIS1" :  '',
            "PARAM_LETRA2" : '',
            "PARAM_ESTESUR1" : '',
            "PARAM_NUMERO2" : '',
            "PARAM_LETRA3" : '',
            "PARAM_BIS2" : '',
            "PARAM_LETRA4" : '',
            "PARAM_NUMERO3" : '',
            "PARAM_ESTESUR2" : ''
          }
        )
        }

        this.formService.enviarAnalisisGrupoFamiliar(listaPersonasAnalizar).subscribe((data: {}) => {

          let respuestaAnalisisGrupo: any = data;

          let arrayErrores: any = []
          let habilitarGrupo = 0;

          for (let i = 0; i < respuestaAnalisisGrupo.length; i++) {
            if( respuestaAnalisisGrupo[i].p_IDENTIFICADOR != 10 && respuestaAnalisisGrupo[i].p_IDENTIFICADOR != 4 && respuestaAnalisisGrupo[i].p_IDENTIFICADOR != 11 ){

            habilitarGrupo = habilitarGrupo + 1  
            arrayErrores.push({p_ID_CIUDANANO: respuestaAnalisisGrupo[i].p_ID_CIUDANANO, p_IDENTIFICADOR: respuestaAnalisisGrupo[i].p_IDENTIFICADOR })
          }
          }
          if( habilitarGrupo > 0 ){
            this.settings.showLoadingRequest(false);

            this.listadoErrorGrupo = arrayErrores;

            $('#modalErrores').modal('show');

          } else {

            let observables = [];
            this.listadoPersonasHogarRegistraduria = [];



            for( let i = 0; i < listaPersonasAnalizar.length; i++ ) {
              // console.log(data[i].id, 'mostrando los id')
               observables.push(this.formService.consultarDocumento(listaPersonasAnalizar[i].PARAM_DOCUMENTO_PERSONA))
         
             }

             console.log('observablessss', observables)
             forkJoin(observables).subscribe((results: any) => {
              // console.log(results, 'books: booksJson, movies: moviesJson'); // { books: booksJson, movies: moviesJson }

              console.log(results, 'resultados consulta documentos')
              this.listadoPersonasHogarRegistraduria = results
         
            });
 

            this.listadoErrorGrupo = [];
            this.settings.showLoadingRequest(false);
            this.myStepper.next();

            const toastOptions: ToastOptions = {
              title: 'Validación Integrantes del Hogar ',
              msg: `Se ha validado exitosamente los integrantes del Hogar.`,
              showClose: true,
              timeout: 5000,
              theme: 'bootstrap'
          };
            this.toastyService.success(toastOptions);

          }
        });

      }
    } else {
      this.myStepper.next();

    }
  }

  getDocumento(estado) {
    if (estado != null) {
      switch (estado) {
        case '1':
          return "Cédula de Ciudadanía";
        case '2':
          return "Tarjeta de Identidad";
        case '3':
          return "Documento de Ciudadano Extranjero";
        case '4':
          return "Registro Civil";
        case '5':
          return "Documento Nacional de Identidad";
        case '6':
          return "Pasaporte";
        case '7':
          return "Pasaporte";  
        case '8':
          return "Permiso Especial de Permanencia";         
          default:
          break;
      }
    }
  }

  getErroresValidacion(estado) {
    if (estado != null) {
      switch (estado) {
        case 1:
          return "La persona ya está registrada en la base de datos de encuesta Sisbén de Bogotá. Por favor acérquese a cualquier punto de atención Sisbén de la Red CADE, para que se adelante el trámite correspondiente. Debe presentar su cédula original, fotocopia legible del documento de identidad de todos los integrantes del hogar y un recibo del servicio público de su residencia actual.";
        case 2:
          return "La persona ya tiene asociada una solicitud de encuesta Sisbén en Bogotá. Para mayor información puede ingresar a la página: www.sdp.gov.co enlace Sisbén - Consultas - Consulte Aquí su Trámite: digite el número de solicitud y documento o comuníquese a la línea 195 o al PBX 6013358000 - Opción 2.";
        case 3:
          return "3";
        case 4:
          return "4";
        case 5:
          return "La persona ya tiene asociada una solicitud de encuesta Sisbén en Bogotá. Para mayor información puede ingresar a la página: www.sdp.gov.co enlace Sisbén - Consultas - Consulte Aquí su Trámite: digite el número de solicitud y documento o comuníquese a la línea 195 o al PBX 6013358000 - Opción 2.";
        case 10:
          return "10";
        case 11:
          return "11";  
          default:
          break;
      }
    }
  }


  getParentesco(estado) {
    if (estado != null) {
      switch (estado) {
        case '1':
          return "Jefe del hogar";
        case '2':
          return "Cónyuge o compañero(a)";
        case '3':
          return "Hijo(a), hijastro(a), hijo(a) adoptivo(a)";
        case '4':
          return "Nieto(a)";
        case '5':
          return "Padre, madre, padrastro, madrastra";
        case '6':
          return "Hermano(a)";
        case '7':
          return "Yerno / Nuera";  
        case '8':
          return "Abuelo(a)"; 
        case '9':
          return "Suegro(a)";  
        case '10':
          return "Tío(a)"; 
        case '11':
          return "Sobrino(a)";  
        case '12':
          return "Primo(a)"; 
        case '13':
          return "Cuñado(a)";  
        case '14':
          return "Otro pariente"; 
        case '15':
          return "Empleado(a) de servicio doméstico";
        case '16':
          return "Pariente del servicio doméstico"; 
        case '17':
          return "Pensionista";                                              
          default:
          break;
      }
    }
  }


  public resolved(captchaResponse: string) {
    this.habilitarCaptcha = this.validadorFormulario(this.respuestaRecaptcha) ? false : true
    this.respuestaRecaptcha = captchaResponse;
    this.validadorFormulario(this.respuestaRecaptcha) ?  this.direccionesUnidades.get('captcha').setValue('null') : this.direccionesUnidades.get('captcha').setValue(null)
  }


  validadorFormulario(data){
    let resultado = true;
    if (data === undefined || data === null){
      resultado = false;
    }
    this.habilitarCaptcha = resultado;
    return resultado;
  }



    public descarga() {
    //var data = document.getElementById("contentToConvert");

    const data = this.reportContent.nativeElement;

    html2canvas(data).then(canvas => {
      // Few necessary setting options
      var margin = 20;
      var imgWidth = 210 - 2 * margin; 
      var pageHeight = 295;  
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;

      const contentDataURL = canvas.toDataURL("image/png");
      let pdf = new jsPDF("p", "mm", "letter"); // A4 size page of PDF
      var position = -0;
      pdf.addImage(contentDataURL, "PNG", margin, position, imgWidth, imgHeight);
      pdf.save("reporte_solicitud_encuesta.pdf"); // Generated PDF
    });
  }

   createCaptcha() {
    
    //clear the contents of captcha div first 
    document.getElementById('captcha').innerHTML = "";
   //(<HTMLInputElement>document.getElementById('captcha')).value = '';
    var charsArray = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@!#$%^&*";
    var lengthOtp = 8;
    var captcha = [];
    for (var i = 0; i < lengthOtp; i++) {
      //below code will not allow Repetition of Characters
      var index = Math.floor(Math.random() * charsArray.length + 1); //get the next character from the array
      if (captcha.indexOf(charsArray[index]) == -1)
        captcha.push(charsArray[index]);
      else i--;
    }
    var canv = document.createElement("canvas");
    canv.id = "captcha";
    canv.width = 250;
    canv.height = 50;
    var ctx = canv.getContext("2d");
    ctx.font = "25px Georgia";
    ctx.strokeText(captcha.join(""), 0, 30);
    //storing captcha so that can validate you can save it somewhere else according to your specific requirements
    this.code = captcha.join("");
    (<HTMLInputElement>document.getElementById('captcha')).appendChild(canv); // adds the canvas to the body element
  }


   validateCaptcha(event) {

    event.preventDefault();
    //debugger

    var inputValue = (<HTMLInputElement>document.getElementById('cpatchaTextBox')).value;
    if (inputValue == this.code) {
     // alert("Valid Captcha")

      this.habilitarCaptcha = true;
      this.direccionesUnidades.get('captcha').setValue('null')

      const toastOptions: ToastOptions = {
        title: 'Validación Captcha',
        msg: `Se ha validado exitosamente el captcha.`,
        showClose: true,
        timeout: 5000,
        theme: 'bootstrap'
    };
      this.toastyService.success(toastOptions);

    }else{
     // alert("Invalid Captcha. try Again");
      this.habilitarCaptcha = false;
      this.direccionesUnidades.get('captcha').setValue('')

      const toastOptions: ToastOptions = {
        title: 'Error',
        msg: `No se ha validado exitosamente el captcha.`,
        showClose: true,
        timeout: 5000,
        theme: 'bootstrap'
    };
      this.toastyService.error(toastOptions);
      this.createCaptcha();
    }
  }




  public downloadPDFaa() {
  
    var data = document.getElementById('htmlData');
        html2canvas(data).then(canvas => {
            var imgWidth = 208;
            var pageHeight = 295;
            var imgHeight = canvas.height * imgWidth / canvas.width;
            var heightLeft = imgHeight;

            const contentDataURL = canvas.toDataURL('image/png')
            let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
            var position = 0;
            pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
            //save file with specific name
            pdf.save("Wallet.pdf");
        });
  }

  downloadPdf(pdfUrl: string, pdfName: string ) {
    var FileSaver = require('file-saver');
    FileSaver.saveAs(pdfUrl, pdfName);
  }

  enviarRegistro() {
    Swal.fire({
      title: '<strong>Atención</strong>',
      icon: 'warning',
      html:
        '<div class="col-md-12 mt-3 text-left">' +/*
        `<h6><b class="textModalSend-v1">SOLICITANTE: </b> <span class="ml-3 textModalSend"> ${this.modelRegistroSolicitudes.primerNombreCiudadano + ' ' + (this.modelRegistroSolicitudes.segundoNombreCiudadano ? this.modelRegistroSolicitudes.segundoNombreCiudadano : '') + ' ' + this.modelRegistroSolicitudes.primerApellidoCiudadano + ' ' + (this.modelRegistroSolicitudes.segundoApellidoCiudadano ? this.modelRegistroSolicitudes.segundoApellidoCiudadano : '')} </span></h6>`+
        `<h6><b class="textModalSend-v1">DOCUMENTO: </b> <span class="ml-3 textModalSend">${this.modelRegistroSolicitudes.numeroDocumentoCiudadano} </span></h6>`+
        `<h6><b class="textModalSend-v1">DIRECCIÓN: </b>  <span class="ml-3 textModalSend">${this.PARAMETRO_DIRECCION + ' ' + this.direccion.complementoDireccion} </span></h6>`+
        `<h6><b class="textModalSend-v1">TELÉFONO:</b> <span class="ml-3 textModalSend">${this.modelRegistroSolicitudes.telefonoCelularCiudadano}</span></h6>`+
        `<h6><b class="textModalSend-v1">CORREO ELECTRÓNICO:  </b> <span class="ml-3 textModalSend">${this.modelRegistroSolicitudes.emailCiudadano}</span></h6>`+
        `<h6><b class="textModalSend-v1">LOCALIDAD: </b> <span class="ml-3 textModalSend">${this.PARAMETRO_LOCALIDAD}</span></h6>`+
        `<h6><b class="textModalSend-v1">SECTOR CATASTRAL:  </b> <span class="ml-3 textModalSend">${this.PARAMETRO_BARRIO}</span></h6>`+
        `<h6><b class="textModalSend-v1">ESTRATO: </b> <span class="ml-3 textModalSend">${this.PARAMETRO_ESTRATO}</span></h6>`+
        */
        `<h5 class="mt-4 text-center"><b>¿Está seguro de enviar la solicitud?</b></h5>`+
        `<h5 class="mt-3 text-danger text-center"><b>Recuerde que una vez enviada la información no se podrá modificar</b></h5>`+


        '</div>',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-send mr-2"></i> Aceptar',
      cancelButtonText:
        '<i class="fa fa-close"></i> Cancelar',
    }).then((result) => {
  
      if (result.value) {
      this.settings.showLoadingRequest(true);
            let datosSolicitante = {
              "PARAM_NOMBRE1" : this.modelRegistroSolicitudes.primerNombreCiudadano.toUpperCase(), 
              "PARAM_NOMBRE2" : this.modelRegistroSolicitudes.segundoNombreCiudadano ? this.modelRegistroSolicitudes.segundoNombreCiudadano.toUpperCase(): '',
              "PARAM_APELLIDO1" : this.modelRegistroSolicitudes.primerApellidoCiudadano.toUpperCase(),
              "PARAM_APELLIDO2" : this.modelRegistroSolicitudes.segundoApellidoCiudadano ? this.modelRegistroSolicitudes.segundoApellidoCiudadano.toUpperCase(): '',
              "PARAM_TIPO_DOCUMENTO" : this.modelRegistroSolicitudes.idTipoDocumentoCiudadano,
              "PARAM_NO_DOCUMENTO" : this.modelRegistroSolicitudes.numeroDocumentoCiudadano,
              "PARAM_SEXO" : this.modelRegistroSolicitudes.idTSexoCiudadano.toUpperCase(),
              "PARAM_FECHA_NACIMIENTO" :  moment(this.modelRegistroSolicitudes.fechaNacimientoCiudadano).format('DD/MM/YYYY') ,
              "PARAM_CORREO_ELECTRONICO" : this.modelRegistroSolicitudes.emailCiudadano.toUpperCase(),
              "PARAM_TELEFONO" : this.modelRegistroSolicitudes.telefonoCelularCiudadano,
              "PARAM_TELEFONO_2" : this.modelRegistroSolicitudes.telefonoFijoCiudadano,
              "PARAM_PARENTESCO" : Number(this.modelRegistroSolicitudes.idParentesco), 
              "PARAM_DIRECCION" : this.PARAMETRO_DIRECCION + ' ' + ( this.direccion.complementoDireccion ? this.direccion.complementoDireccion.toUpperCase() : '' ),
              "PARAM_CUNU" : String(this.PARAMETRO_CUNU)
            }

            this.formService.enviarRegistroSolicitante(datosSolicitante).subscribe((data: {}) => {

              let respuestaSaveSolicitante: any = data;
              let listaParametrosHogar: any = [];
              this.PARAMETRO_NUMERO_CONSECUTIVO = respuestaSaveSolicitante.p_NUMERO_CONSECUTIVO;


              for (let i = 0; i < this.listadoPersonasHogar.length; i++) {
      


              for (let j = 0; j < this.listadoPersonasHogarRegistraduria.length; j++) {

                if (Number(this.listadoPersonasHogar[i].numeroDocumentoCiudadano) === (this.listadoPersonasHogar[j].Nuip)){

                  if(this.listadoPersonasHogar[j].EstadoCedula === '0'){


                    listaParametrosHogar.push(
                      {
                        "PARAM_INTERNO_CIUDADANO" : respuestaSaveSolicitante.p_INTERNO_CIUDADANO, 
                        "PARAM_NOMBRE1" : this.listadoPersonasHogarRegistraduria[j].PrimerNombre.toUpperCase(), 
                        "PARAM_NOMBRE2" : this.listadoPersonasHogarRegistraduria[j].SegundoNombre ? this.listadoPersonasHogarRegistraduria[j].SegundoNombre.toUpperCase() : '',
                        "PARAM_APELLIDO1" : this.listadoPersonasHogarRegistraduria[j].PrimerApellido.toUpperCase(),
                        "PARAM_APELLIDO2" : this.listadoPersonasHogarRegistraduria[j].SegundoApellido ? this.listadoPersonasHogarRegistraduria[j].SegundoApellido.toUpperCase(): '',
                        "PARAM_TIPO_DOCUMENTO" : this.listadoPersonasHogar[i].idTipoDocumentoCiudadano,
                        "PARAM_NO_DOCUMENTO" : this.listadoPersonasHogar[i].numeroDocumentoCiudadano,
                        "PARAM_SEXO" : this.listadoPersonasHogar[i].idTSexoCiudadano.toUpperCase(),
                        "PARAM_FECHA_NACIMIENTO" : moment(this.listadoPersonasHogar[i].fechaNacimientoCiudadano).format('DD/MM/YYYY'),
                        "PARAM_CORREO_ELECTRONICO" : this.modelRegistroSolicitudes.emailCiudadano.toUpperCase(),
                        "PARAM_PARENTESCO" : Number(this.listadoPersonasHogar[i].idParentesco), 
                      }
                    )

                  } else {

                    listaParametrosHogar.push(
                      {
                        "PARAM_INTERNO_CIUDADANO" : respuestaSaveSolicitante.p_INTERNO_CIUDADANO, 
                        "PARAM_NOMBRE1" : this.listadoPersonasHogar[i].primerNombreCiudadano.toUpperCase(), 
                        "PARAM_NOMBRE2" : this.listadoPersonasHogar[i].segundoNombreCiudadano ? this.listadoPersonasHogar[i].segundoNombreCiudadano.toUpperCase() : '',
                        "PARAM_APELLIDO1" : this.listadoPersonasHogar[i].primerApellidoCiudadano.toUpperCase(),
                        "PARAM_APELLIDO2" : this.listadoPersonasHogar[i].segundoApellidoCiudadano ? this.listadoPersonasHogar[i].segundoApellidoCiudadano.toUpperCase(): '',
                        "PARAM_TIPO_DOCUMENTO" : this.listadoPersonasHogar[i].idTipoDocumentoCiudadano,
                        "PARAM_NO_DOCUMENTO" : this.listadoPersonasHogar[i].numeroDocumentoCiudadano,
                        "PARAM_SEXO" : this.listadoPersonasHogar[i].idTSexoCiudadano.toUpperCase(),
                        "PARAM_FECHA_NACIMIENTO" : moment(this.listadoPersonasHogar[i].fechaNacimientoCiudadano).format('DD/MM/YYYY'),
                        "PARAM_CORREO_ELECTRONICO" : this.modelRegistroSolicitudes.emailCiudadano.toUpperCase(),
                        "PARAM_PARENTESCO" : Number(this.listadoPersonasHogar[i].idParentesco), 
                      }
                    )
                  }

                }
    
                }

              }

              this.formService.enviarRegistroGrupoFamiliar(listaParametrosHogar).subscribe((data: {}) => {
                this.settings.showLoadingRequest(false);  

                $("#modalReporte").modal("show");


                setTimeout(() => {
                  this.descarga();
                  //this.refresh();
                  
                        }, 1500);
 
              });
  
            });
          
        
      }
    })


  }

  refresh(): void {
   // window.location.reload();
    let urlRedirect = String(window.location.origin);
    //window.location.assign(urlRedirect);
   // window.location.reload();
    window.location.href = urlRedirect;
}


  guardarDireccionVivienda() {
    this.submittedDireccionVivienda = true;
    if (this.direccionViviendaForm.invalid) {
        return;
    } 
    if(this.direccionViviendaForm.valid){

      this.settings.showLoadingRequest(true);  

      let datosDireccion = {
        "PARAM_DOCUMENTO_PERSONA" : this.modelRegistroSolicitudes.numeroDocumentoCiudadano, 
        "PARAM_CALLE" : this.direccion.nombreCalle != '' &&  this.direccion.nombreCalle != null  ?  this.nombreCalle[this.direccion.nombreCalle].value : '',
        "PARAM_NUMERO1" : this.direccion.numeroPrincipal,
        "PARAM_LETRA1" : this.direccion.letraPrincipal != '' &&  this.direccion.letraPrincipal != null  ?  this.letras[this.direccion.letraPrincipal].nombre : '',
        "PARAM_BIS1" :  this.direccion.bisPrincipal != '' &&  this.direccion.bisPrincipal != null  ? this.bisReferencia[this.direccion.bisPrincipal].nombre : '',
        "PARAM_LETRA2" : this.direccion.letraPrincipal2 != '' &&  this.direccion.letraPrincipal2 != null  ?  this.letras[this.direccion.letraPrincipal2].nombre : '',
        "PARAM_ESTESUR1" : this.direccion.referenciaCardinal != '' &&  this.direccion.referenciaCardinal != null  ? this.referenciaCardinal[this.direccion.referenciaCardinal].value : '',
        "PARAM_NUMERO2" : this.direccion.numeroSecundario,
        "PARAM_LETRA3" : this.direccion.letraSecundaria != '' &&  this.direccion.letraSecundaria != null  ?  this.letras[this.direccion.letraSecundaria].nombre : '',
        "PARAM_BIS2" : this.direccion.bisSecundario != '' &&  this.direccion.bisSecundario != null  ? this.bisReferencia[this.direccion.bisSecundario].nombre  : '',
        "PARAM_LETRA4" : this.direccion.letraPrincipal4 != '' &&  this.direccion.letraPrincipal4 != null  ?  this.letras[this.direccion.letraPrincipal4].nombre : '',
        "PARAM_NUMERO3" : this.direccion.numeroCasa,
        "PARAM_ESTESUR2" : this.direccion.referenciaCardinal2 != '' &&  this.direccion.referenciaCardinal2 != null  ? this.referenciaCardinal[this.direccion.referenciaCardinal2].value : '',
      }

      
    
    
      this.formService.enviarAnalisisSolicitante(datosDireccion).subscribe((data: {}) => {
    
        let analisisDireccion : any = data;
    
        if (analisisDireccion.p_VALIDACION === 1 ){
    
          this.settings.showLoadingRequest(false);  
          this.PARAMETRO_CUNU =analisisDireccion.p_CUNU;
          this.PARAMETRO_DIRECCION =analisisDireccion.p_DIRECCION;
          this.PARAMETRO_LOCALIDAD =analisisDireccion.p_R_LOCALIDAD;
          this.PARAMETRO_BARRIO =analisisDireccion.p_R_NOMBARIO;
          this.PARAMETRO_ESTRATO =analisisDireccion.p_R_ESTRATO;


          const toastOptions: ToastOptions = {
            title: 'Validación Dirección Vivienda',
            msg: `Se ha validado exitosamente la dirección de la vivienda.`,
            showClose: true,
            timeout: 5000,
            theme: 'bootstrap'
        };
          this.toastyService.success(toastOptions);

          this.modulo_paso_3.get('validarPaso3').setValue('true')
          this.myStepper.next();
          this.conteoIntentos = 0;
    
    
        } else if (analisisDireccion.p_VALIDACION === 0){
          this.conteoIntentos = this.conteoIntentos + 1
    
          this.settings.showLoadingRequest(false); 
          this.PARAMETRO_CUNU ='';
          this.PARAMETRO_DIRECCION ='';
          this.PARAMETRO_LOCALIDAD ='';
          this.PARAMETRO_BARRIO ='';
          this.PARAMETRO_ESTRATO ='';
          this.modulo_paso_3.get('validarPaso3').setValue('')

          if (this.conteoIntentos >= 3){

            Swal.fire({
              title: '¡Atención!',
              text: "El sistema no validó su dirección en la zona urbana de Bogotá, por favor dirigirse al punto de atención más cercano a su residencia.",
              icon: 'error',
              showCancelButton: false,
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Aceptar',
              cancelButtonColor: '#d33',
              cancelButtonText: 'No',
            }).then((result) => {
      
              this.conteoIntentos = 0;
              if (result.value) {
                let urlRedirectDir = String('http://www.sdp.gov.co/gestion-estudios-estrategicos/sisben/canales-atencion');
                window.location.href = urlRedirectDir;
              }
            })

          } else if (this.conteoIntentos === 2) {
            Swal.fire(
              '¡Atención!',
              'El sistema aún no válida la dirección. Ingrésela como aparece en un recibo público de la vivienda.',
              'error'
            );
          } else if(this.conteoIntentos === 1) {
            Swal.fire(
              '¡Atención!',
              'El sistema no válida la dirección, por favor verifique los datos digitados.',
              'error'
            );
          }
    
    
          

    
        }  else {
    
          this.settings.showLoadingRequest(false); 
          this.PARAMETRO_CUNU ='';
          this.PARAMETRO_DIRECCION ='';
          this.PARAMETRO_LOCALIDAD ='';
          this.PARAMETRO_BARRIO ='';
          this.PARAMETRO_ESTRATO ='';
          this.modulo_paso_3.get('validarPaso3').setValue('')
    
          Swal.fire(
            '¡Atención!',
            'El solicitante debe dirijirse al CADE mas cercano.',
            'error'
          );
        }
    
    
    
      });
    }


    
  }

  resetGrupoHogar(){
    this.establecimiento = {};
    this.modulo_establecimiento.reset();
    this.establecimiento.idTipoDocumentoCiudadano = '';
    this.establecimiento.idParentesco = '';
    this.establecimiento.idTSexoCiudadano = '';
    this.modulo_establecimiento.updateValueAndValidity();
  }

  nuevoIntegranteHogar(selected){
    this.isEditPuntosVentas = selected;
    this.establecimiento = {};
    this.modulo_establecimiento.reset();
    this.establecimiento.idTipoDocumentoCiudadano = '';
    this.establecimiento.idParentesco = '';
    this.establecimiento.idTSexoCiudadano = '';
    this.modulo_establecimiento.get('idTipoDocumentoCiudadano').setValue('');
    this.modulo_establecimiento.get('idParentesco').setValue('');
    this.modulo_establecimiento.get('idTSexoCiudadano').setValue('');


    this.modulo_establecimiento.updateValueAndValidity();

    this.paginatorPuntosVentas._intl.itemsPerPageLabel = 'Registros por pagina:';
    this.paginatorPuntosVentas._intl.firstPageLabel = 'Primera pagina';
    this.paginatorPuntosVentas._intl.previousPageLabel = 'Pagina anterior';
    this.paginatorPuntosVentas._intl.nextPageLabel = 'Pagina siguiente';
    this.paginatorPuntosVentas._intl.lastPageLabel = 'Ultima pagina';
    this.dataSourcePuntosVentas = new MatTableDataSource(this.listadoPersonasHogar);
    this.dataSourcePuntosVentas.paginator = this.paginatorPuntosVentas;
    this.dataSourcePuntosVentas.sort = this.sortPuntosVentas;
    this.loading = false;
  }

  eliminarRegistroPuntosVenta(payload, data) {

      Swal.fire({
        title: '¿Está seguro de eliminar este integrante del hogar?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Si, Eliminarlo !',
        cancelButtonColor: '#d33',
        cancelButtonText: 'No',
      }).then((result) => {

        if (result.value) {

          this.listadoPersonasHogar.forEach( (item, index) => {
            if(index === data) {
              this.listadoPersonasHogar.splice(index,1)
              Swal.fire(
                'Eliminado!',
                'Se ha eliminado exitosamente el integrante del hogar.',
                'success'
              );
              this.nuevoCargue()
            };
            if(this.listadoPersonasHogar.length === 0){
              this.personasHogar.get('listadoPersonas').setValue([])
            }  
          });

        }
      })
  }

resetDireccion (){
  //this.establecimiento.direccion = '';
  this.direccion.nombreCalle = '';
  this.direccion.numeroPrincipal = '';
  this.direccion.letraPrincipal = '';
  this.direccion.bisPrincipal = '';
  this.direccion.referenciaCardinal = '';
  this.direccion.numeroSecundario = '';
  this.direccion.letraSecundaria = '';
  this.direccion.bisSecundario = '';
  this.direccion.numeroCasa = '';
  this.direccion.complementoDireccion = '';
  this.direccion.letraPrincipal2 = '';
  this.direccion.letraPrincipal4 = '';
  this.direccion.referenciaCardinal2 = '';
  this.direccion.localidad = '';
  this.direccion.barrio = '';
  this.direccion.upz = '';

}

generateDireccion() {

  this.submittedDirecciones = true;

  if (this.direcciones.invalid) {
    return;
  }

  let addr = '';
  if (this.nombreCalle[this.direccion.nombreCalle] !== undefined) {
    addr = this.nombreCalle[this.direccion.nombreCalle].nombre + ' ' + this.direccion.numeroPrincipal;
  }
  if (this.letras[this.direccion.letraPrincipal] !== undefined) {
    addr += ' ' + this.letras[this.direccion.letraPrincipal].nombre;
  }
  if (this.direccion.bisPrincipal === '0') {
    addr += ' Bis';
  }else {
    addr += '';
  }

  if (this.letras[this.direccion.letraPrincipal2] !== undefined) {
    addr += ' ' + this.letras[this.direccion.letraPrincipal2].nombre;
  }

  if (this.referenciaCardinal[this.direccion.referenciaCardinal] !== undefined) {
    addr += ' ' + this.referenciaCardinal[this.direccion.referenciaCardinal].nombre;
  }
  if (this.direccion.numeroSecundario !== null) {
    addr += ' # ' + this.direccion.numeroSecundario;
  }
  if (this.letras[this.direccion.letraSecundaria] !== undefined) {
    addr += ' ' + this.letras[this.direccion.letraSecundaria].nombre;
  }

  if (this.letras[this.direccion.letraPrincipal4] !== undefined) {
    addr += ' ' + this.letras[this.direccion.letraPrincipal4].nombre;
  }
  if (this.direccion.bisSecundario === '0') {
    addr += ' Bis -';
  }else {
    addr += ' -';
  }
  if (this.direccion.numeroCasa !== null) {
    addr += ' ' + this.direccion.numeroCasa;
  }

  if (this.referenciaCardinal[this.direccion.referenciaCardinal2] !== undefined) {
    addr += ' ' + this.referenciaCardinal[this.direccion.referenciaCardinal2].nombre;
  }



  if (this.direccion.inmueble !== null) {
    addr += ' ' + this.direccion.inmueble.toUpperCase();
  }

  if (this.direccion.numeroInmueble !== null) {
    addr += ' ' + this.direccion.numeroInmueble;
  }

  if (this.direccion.complementoDireccion !== null) {
    addr += ' ' + this.direccion.complementoDireccion.toUpperCase();
  }


  if (addr.length > 10) {
    this.direccionCompleta = addr;
  }

  $('#modalDireccion').modal('hide');
  this.submittedDirecciones = false;

  console.log(this.direccionCompleta, 'direccion')

}

guardarDatosSolicitante() {
  this.submittedDireccionesUnidades = true;
  if (this.direccionesUnidades.invalid) {
    return;
  }
  /*if (!this.validadorFormulario(this.respuestaRecaptcha)) {
    return;
  } */


  let dataEdadTemp
  dataEdadTemp = this.calculaEdad(this.modelRegistroSolicitudes.fechaNacimientoCiudadano)

  console.log(dataEdadTemp, this.modelRegistroSolicitudes.idTipoDocumentoCiudadano, 'CALCULANDO EDAD' )

  if (dataEdadTemp === 'old' && (this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '1' || this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '3' || this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '8' || this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '6' || this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '6')){

    //this.grabarHogarArray()

  } else if (dataEdadTemp === 'young' && (this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '2' || this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '3' || this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '6')){

    //this.grabarHogarArray()

  } else if (dataEdadTemp === 'child' && (this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '4' || this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '5' || this.modelRegistroSolicitudes.idTipoDocumentoCiudadano === '6')){

   //this.grabarHogarArray()

  } else {


    Swal.fire({
      title: '¡Atención!',
      icon: 'warning',
      html:
        `El tipo de documento no es coherente con la edad. Por favor verifique la información digitada. 
        <br>
        *Validar: tipo de documento por rangos de edad: <br>
        <ul>
        <li>Mayores de 18 años (CC, PEP, Salvoconducto, CE)</li>
        <li>Entre 7 y 17 años (TI, PEP, Salvoconducto, CE)</li>
        <li>Menores de 7 años (RC, DNI, Pasaporte, PEP, Salvoconducto)</li>
      </ul>`,
      showCloseButton: false,
      showCancelButton: false,
      focusConfirm: true,
      confirmButtonText:
        ' Aceptar',
    })
    return

  }

  if(this.direccionesUnidades.valid){
    this.guardarInicialPersona()
  }
}



guardarInicialPersona() {

  this.settings.showLoadingRequest(true);  




  let datosAnalisis = {
    "PARAM_DOCUMENTO_PERSONA" : this.modelRegistroSolicitudes.numeroDocumentoCiudadano, 
    "PARAM_CALLE" : '',
    "PARAM_NUMERO1" : '',
    "PARAM_LETRA1" : '',
    "PARAM_BIS1" :  '',
    "PARAM_LETRA2" : '',
    "PARAM_ESTESUR1" : '',
    "PARAM_NUMERO2" : '',
    "PARAM_LETRA3" : '',
    "PARAM_BIS2" : '',
    "PARAM_LETRA4" : '',
    "PARAM_NUMERO3" : '',
    "PARAM_ESTESUR2" : ''
  }


  this.formService.enviarAnalisisSolicitante(datosAnalisis).subscribe((data: {}) => {

    let analisisSolicitante : any = data;

    if (analisisSolicitante.p_IDENTIFICADOR === 10 || analisisSolicitante.p_IDENTIFICADOR === 4 || analisisSolicitante.p_IDENTIFICADOR === 11 || analisisSolicitante.p_IDENTIFICADOR === 3){

      this.settings.showLoadingRequest(false);  
      this.modulo_paso_1.get('validarPaso1').setValue('true')
      this.myStepper.next();

      const toastOptions: ToastOptions = {
        title: 'Validación Solicitante',
        msg: `Se ha validado exitosamente el solicitante.`,
        showClose: true,
        timeout: 5000,
        theme: 'bootstrap'
    };
      this.toastyService.success(toastOptions);

      this.formService.consultarDocumento(this.modelRegistroSolicitudes.numeroDocumentoCiudadano).subscribe((data: {}) => {
  
        let dataReceive: any = data;
  
        if (dataReceive.PrimerApellido != null && dataReceive.PrimerNombre != null && dataReceive.EstadoCedula != null && dataReceive.FechaExpedicion != null){

          this.modelRegistroSolicitudes.primerNombreCiudadano = dataReceive.PrimerNombre;
          this.modelRegistroSolicitudes.segundoNombreCiudadano = dataReceive.SegundoNombre;
          this.modelRegistroSolicitudes.primerApellidoCiudadano = dataReceive.PrimerApellido;
          this.modelRegistroSolicitudes.segundoApellidoCiudadano = dataReceive.SegundoApellido;

        } else {

          /*const toastOptions: ToastOptions = {
            title: 'Validación Fallida',
            msg: `No se ha podido validar la información del solicitante. Por favor registre su número de documento, nombres, apellidos y fecha de nacimiento correctamente.`,
            showClose: true,
            timeout: 5000,
            theme: 'bootstrap'
        };
          this.toastyService.error(toastOptions);*/
        }
  
      }); 


    } else if (analisisSolicitante.p_IDENTIFICADOR === 1){

      this.settings.showLoadingRequest(false); 
      this.modulo_paso_1.get('validarPaso1').setValue('')

      
      Swal.fire(
        '¡Atención!',
        'La persona ya está registrada en la base de datos de encuesta Sisbén de Bogotá. Por favor acérquese a cualquier punto de atención Sisbén de la Red CADE, para que se adelante el trámite correspondiente. Debe presentar su cédula original, fotocopia legible del documento de identidad de todos los integrantes del hogar y un recibo del servicio público de su residencia actual.',
        'error'
      );

    }  else if (analisisSolicitante.p_IDENTIFICADOR === 2){

      this.settings.showLoadingRequest(false); 
      this.modulo_paso_1.get('validarPaso1').setValue('')

      
      Swal.fire(
        '¡Atención!',
        'La persona ya tiene asociada una solicitud de encuesta Sisbén en Bogotá. Para mayor información puede ingresar a la página: www.sdp.gov.co enlace Sisbén - Consultas - Consulte Aquí su Trámite: digite el número de solicitud y documento o comuníquese a la línea 195 o al PBX 6013358000 - Opción 2.',
        'error'
      );

    } else if (analisisSolicitante.p_IDENTIFICADOR === 5){

      this.settings.showLoadingRequest(false); 
      this.modulo_paso_1.get('validarPaso1').setValue('')

      
      Swal.fire(
        '¡Atención!',
        'La persona ya tiene asociada una solicitud de encuesta Sisbén en Bogotá. Para mayor información puede ingresar a la página: www.sdp.gov.co enlace Sisbén - Consultas - Consulte Aquí su Trámite: digite el número de solicitud y documento o comuníquese a la línea 195 o al PBX 6013358000 - Opción 2.',
        'error'
      );

    } else {

      this.settings.showLoadingRequest(false); 
      this.modulo_paso_1.get('validarPaso1').setValue('')

      Swal.fire(
        '¡Atención!',
        'El solicitante debe dirijirse al CADE mas cercano.',
        'error'
      );
    }


  });

}



calculaEdad(payload) {
  var a = moment();
  var b = moment(payload);

  var years = a.diff(b, "year");
  b.add(years, "years");
  var months = a.diff(b, "months");
  b.add(months, "months");
  var days = a.diff(b, "days");

  if (years >= 0 && years <= 7) {

      return 'child';

    } else if (years >= 8 && years <= 17){
    
      return 'young';

    } else if (years >= 18 && years <= 120){

      return 'old';

    }
}

onlyNumbers(e) {
  const event = e || window.event;

  const key = event.keyCode || event.which;
  const text = String.fromCharCode(key);

  var regex = /^\d+$/;
  if (!regex.test(text)) {
    event.preventDefault();
  }
}




}


