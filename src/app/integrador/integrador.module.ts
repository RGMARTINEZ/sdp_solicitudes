import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpService } from './services/http.service';
import { MensajeService} from './services/mensaje.service';
import { AuthService } from './services/auth.service';
import { ConfiguracionService } from './services/configuracion.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    HttpService,
    MensajeService,
    AuthService,
    ConfiguracionService

  ]
})

export class IntegradorModule {}
