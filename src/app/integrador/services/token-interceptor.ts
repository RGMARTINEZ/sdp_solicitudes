import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { environment } from '../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { TokenAdminService } from '../../integrador/services/token-admin.service';

@Injectable()

export class TokenInterceptor implements HttpInterceptor {

  authService;
  refreshTokenInProgress = false;
  tokenRefreshedSource = new Subject();
  tokenRefreshed$ = this.tokenRefreshedSource.asObservable();
  END_POINT = environment.END_POINT_SSO;

  constructor(
      public tokenAdmin: TokenAdminService,
      public http: HttpClient,
      private router: Router
      ) {}

    addAuthHeader(request) {
        const authHeader = 'Bearer ' + this.tokenAdmin.getToken();
        if (authHeader) {
            return request.clone({
                setHeaders: {
                    Authorization: authHeader
                }
            });
        }
        return request;
    }

    refreshToken(): Observable<any> {
        if (this.refreshTokenInProgress) {
            return new Observable(observer => {
                this.tokenRefreshed$.subscribe(() => {
                    observer.next();
                    observer.complete();
                });
            });
        } else {
            this.refreshTokenInProgress = true;
            return this.http.post(this.END_POINT + 'Login.svc/refreshToken', this.tokenAdmin.getRefreshToken()).map(() => {
                    this.refreshTokenInProgress = false;
                    this.tokenRefreshedSource.next();
            });
        }
    }

    logout() {
      this.authService.logout();
      this.router.navigate(['login']);
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
        request = this.addAuthHeader(request);
        return next.handle(request).catch(error => {
            if (error.status === 401) {
                return this.refreshToken()
                    .map(() => {
                        request = this.addAuthHeader(request);
                        return next.handle(request);
                    })
                    .catch (faild => {
                        this.logout();
                        return Observable.throwError(error);
                    });
            }
            if (error.status === 500 ) {
                const mensaje = error.error.message;
                if (mensaje === 'TOKEN NULO' || mensaje === 'RefreshToken invalido!') {
                    this.router.navigate(['login']);
                    this.tokenAdmin.resetToken();
                }
            }
            return Observable.throwError(error);
        });
    }


}
