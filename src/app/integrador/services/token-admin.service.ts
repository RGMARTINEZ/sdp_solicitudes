import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { ConfiguracionService } from '../../integrador/services/configuracion.service';

function getWindow(): any {
  return window;
}

@Injectable()

export class TokenAdminService {

  TOKEN = '';
  public subjectRefreshToken: Subject<any> = new Subject();
  tokenExpirated = true;
  passsTemporal;
  login;

  constructor(public config: ConfiguracionService ) {
  }

  isAuthenticate() {
    return !this.tokenExpirated;
  }

  setToken(dataToken): Promise<boolean> {
    return new Promise(( resolve: (res: boolean) => void, reject: (res: boolean) => void) => {
          const res = false;
          this.nativeWindow.localStorage.token = JSON.stringify(dataToken)

          setTimeout(() => {
              resolve(res);
          }, 3000);
      });

  }

  setTokenExpirated() {
    this.tokenExpirated = true;
  }

  setPassTempo(payload) {
    this.passsTemporal = payload ;
  }

  getPassTempo() {
    return this.passsTemporal;
  }


  setPassLogin(payload) {
    this.login = payload ;
  }

  getPassLogin() {
    return this.login;
  }

  getToken() {
    return JSON.parse(localStorage.getItem('token')) ;
  }

  getRefreshToken() {
    return this.nativeWindow.localStorage.token;
  }

  resetToken() {
    this.TOKEN = '';
    this.nativeWindow.localStorage.removeItem('token');
  }

  expirationTime(time) {
    setTimeout(() => {
      this.setTokenExpirated();
    }, time * 100);
  }

  processRefreshToken() {
    this.subjectRefreshToken.next(this.nativeWindow.localStorage.token);
  }

  get nativeWindow(): any {
    return getWindow();
  }

  public convert(data: any) {
    const date = new Date(data);
    const time = date.getTimezoneOffset() * 60000;
    const last = date.getTime() + time;
    return last;
  }

}
