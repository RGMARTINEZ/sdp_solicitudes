export const LOCALIDADES = [
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA"
    },
    {
        "CODIGO_LOCALIDAD": "17",
        "NOMBRE_LOCALIDAD": "CANDELARIA"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME"
    }
]