export const UPZS = [
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_UPZ": "1",
        "NOMBRE_UPZ": "PASEO DE LOS LIBERTADORES"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_UPZ": "10",
        "NOMBRE_UPZ": "LA URIBE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_UPZ": "11",
        "NOMBRE_UPZ": "SAN CRISTOBAL NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_UPZ": "12",
        "NOMBRE_UPZ": "TOBERIN"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_UPZ": "13",
        "NOMBRE_UPZ": "LOS CEDROS"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_UPZ": "14",
        "NOMBRE_UPZ": "USAQUEN"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_UPZ": "15",
        "NOMBRE_UPZ": "COUNTRY CLUB"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_UPZ": "16",
        "NOMBRE_UPZ": "SANTA BARBARA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_UPZ": "9",
        "NOMBRE_UPZ": "VERBENAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_UPZ": "105",
        "NOMBRE_UPZ": "JARDIN BOTANICO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_UPZ": "116",
        "NOMBRE_UPZ": "ALAMOS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_UPZ": "26",
        "NOMBRE_UPZ": "LAS FERIAS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_UPZ": "29",
        "NOMBRE_UPZ": "MINUTO DE DIOS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_UPZ": "30",
        "NOMBRE_UPZ": "BOYACA REAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_UPZ": "31",
        "NOMBRE_UPZ": "SANTA CECILIA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_UPZ": "72",
        "NOMBRE_UPZ": "BOLIVIA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_UPZ": "73",
        "NOMBRE_UPZ": "GARCES NAVAS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_UPZ": "74",
        "NOMBRE_UPZ": "ENGATIVA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "17",
        "NOMBRE_UPZ": "SAN JOSE DE BAVARIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "18",
        "NOMBRE_UPZ": "BRITALIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "19",
        "NOMBRE_UPZ": "EL PRADO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "2",
        "NOMBRE_UPZ": "LA ACADEMIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "20",
        "NOMBRE_UPZ": "LA ALHAMBRA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "23",
        "NOMBRE_UPZ": "CASA BLANCA SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "24",
        "NOMBRE_UPZ": "NIZA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "25",
        "NOMBRE_UPZ": "LA FLORESTA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "27",
        "NOMBRE_UPZ": "SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "28",
        "NOMBRE_UPZ": "EL RINCON"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "3",
        "NOMBRE_UPZ": "GUAYMARAL"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_UPZ": "71",
        "NOMBRE_UPZ": "TIBABUYES"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_UPZ": "103",
        "NOMBRE_UPZ": "PARQUE SALITRE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_UPZ": "21",
        "NOMBRE_UPZ": "LOS ANDES"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_UPZ": "22",
        "NOMBRE_UPZ": "DOCE DE OCTUBRE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_UPZ": "98",
        "NOMBRE_UPZ": "LOS ALCAZARES"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_UPZ": "100",
        "NOMBRE_UPZ": "GALERIAS"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_UPZ": "101",
        "NOMBRE_UPZ": "TEUSAQUILLO"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_UPZ": "104",
        "NOMBRE_UPZ": "PARQUE SIMON BOLIVAR - CAN"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_UPZ": "106",
        "NOMBRE_UPZ": "LA ESMERALDA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_UPZ": "107",
        "NOMBRE_UPZ": "QUINTA PAREDES"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_UPZ": "109",
        "NOMBRE_UPZ": "CIUDAD SALITRE ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_UPZ": "102",
        "NOMBRE_UPZ": "LA SABANA"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_UPZ": "37",
        "NOMBRE_UPZ": "SANTA ISABEL"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_UPZ": "35",
        "NOMBRE_UPZ": "CIUDAD JARDIN"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_UPZ": "38",
        "NOMBRE_UPZ": "RESTREPO"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_UPZ": "108",
        "NOMBRE_UPZ": "ZONA INDUSTRIAL"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_UPZ": "111",
        "NOMBRE_UPZ": "PUENTE ARANDA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_UPZ": "40",
        "NOMBRE_UPZ": "CIUDAD MONTES"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_UPZ": "41",
        "NOMBRE_UPZ": "MUZU"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_UPZ": "43",
        "NOMBRE_UPZ": "SAN RAFAEL"
    },
    {
        "CODIGO_LOCALIDAD": "17",
        "NOMBRE_LOCALIDAD": "CANDELARIA",
        "CODIGO_UPZ": "94",
        "NOMBRE_UPZ": "LA CANDELARIA"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_UPZ": "36",
        "NOMBRE_UPZ": "SAN JOSE"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_UPZ": "39",
        "NOMBRE_UPZ": "QUIROGA"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_UPZ": "53",
        "NOMBRE_UPZ": "MARCO FIDEL SUAREZ"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_UPZ": "54",
        "NOMBRE_UPZ": "MARRUECOS"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_UPZ": "55",
        "NOMBRE_UPZ": "DIANA TURBAY"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_UPZ": "60",
        "NOMBRE_UPZ": "PARQUE ENTRENUBES"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_UPZ": "63",
        "NOMBRE_UPZ": "EL MOCHUELO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_UPZ": "64",
        "NOMBRE_UPZ": "MONTE BLANCO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_UPZ": "65",
        "NOMBRE_UPZ": "ARBORIZADORA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_UPZ": "66",
        "NOMBRE_UPZ": "SAN FRANCISCO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_UPZ": "67",
        "NOMBRE_UPZ": "LUCERO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_UPZ": "68",
        "NOMBRE_UPZ": "EL TESORO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_UPZ": "69",
        "NOMBRE_UPZ": "ISMAEL PERDOMO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_UPZ": "70",
        "NOMBRE_UPZ": "JERUSALEM"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_UPZ": "88",
        "NOMBRE_UPZ": "EL REFUGIO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_UPZ": "89",
        "NOMBRE_UPZ": "SAN ISIDRO - PATIOS"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_UPZ": "90",
        "NOMBRE_UPZ": "PARDO RUBIO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_UPZ": "97",
        "NOMBRE_UPZ": "CHICO LAGO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_UPZ": "99",
        "NOMBRE_UPZ": "CHAPINERO"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_UPZ": "91",
        "NOMBRE_UPZ": "SAGRADO CORAZON"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_UPZ": "92",
        "NOMBRE_UPZ": "LA MACARENA"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_UPZ": "93",
        "NOMBRE_UPZ": "LAS NIEVES"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_UPZ": "95",
        "NOMBRE_UPZ": "LAS CRUCES"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_UPZ": "96",
        "NOMBRE_UPZ": "LOURDES"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_UPZ": "32",
        "NOMBRE_UPZ": "SAN BLAS"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_UPZ": "33",
        "NOMBRE_UPZ": "SOSIEGO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_UPZ": "34",
        "NOMBRE_UPZ": "20 DE JULIO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_UPZ": "50",
        "NOMBRE_UPZ": "LA GLORIA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_UPZ": "51",
        "NOMBRE_UPZ": "LOS LIBERTADORES"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_UPZ": "52",
        "NOMBRE_UPZ": "LA FLORA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_UPZ": "52",
        "NOMBRE_UPZ": "LA FLORA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_UPZ": "56",
        "NOMBRE_UPZ": "DANUBIO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_UPZ": "57",
        "NOMBRE_UPZ": "GRAN YOMASA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_UPZ": "58",
        "NOMBRE_UPZ": "COMUNEROS"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_UPZ": "59",
        "NOMBRE_UPZ": "ALFONSO LOPEZ"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_UPZ": "60",
        "NOMBRE_UPZ": "PARQUE ENTRENUBES"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_UPZ": "61",
        "NOMBRE_UPZ": "CIUDAD USME"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_UPZ": "42",
        "NOMBRE_UPZ": "VENECIA"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_UPZ": "62",
        "NOMBRE_UPZ": "TUNJUELITO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_UPZ": "49",
        "NOMBRE_UPZ": "APOGEO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_UPZ": "84",
        "NOMBRE_UPZ": "BOSA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_UPZ": "85",
        "NOMBRE_UPZ": "BOSA CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_UPZ": "86",
        "NOMBRE_UPZ": "EL PORVENIR"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_UPZ": "87",
        "NOMBRE_UPZ": "TINTAL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "113",
        "NOMBRE_UPZ": "BAVARIA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "44",
        "NOMBRE_UPZ": "AMERICAS"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "45",
        "NOMBRE_UPZ": "CARVAJAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "46",
        "NOMBRE_UPZ": "CASTILLA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "47",
        "NOMBRE_UPZ": "KENNEDY CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "48",
        "NOMBRE_UPZ": "TIMIZA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "78",
        "NOMBRE_UPZ": "TINTAL NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "79",
        "NOMBRE_UPZ": "CALANDAIMA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "80",
        "NOMBRE_UPZ": "CORABASTOS"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "81",
        "NOMBRE_UPZ": "GRAN BRITALIA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "82",
        "NOMBRE_UPZ": "PATIO BONITO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_UPZ": "83",
        "NOMBRE_UPZ": "LAS MARGARITAS"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_UPZ": "110",
        "NOMBRE_UPZ": "CIUDAD SALITRE OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_UPZ": "112",
        "NOMBRE_UPZ": "GRANJAS DE TECHO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_UPZ": "114",
        "NOMBRE_UPZ": "MODELIA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_UPZ": "115",
        "NOMBRE_UPZ": "CAPELLANIA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_UPZ": "117",
        "NOMBRE_UPZ": "AEROPUERTO EL DORADO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_UPZ": "75",
        "NOMBRE_UPZ": "FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_UPZ": "76",
        "NOMBRE_UPZ": "FONTIBON SAN PABLO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_UPZ": "77",
        "NOMBRE_UPZ": "ZONA FRANCA"
    }
]