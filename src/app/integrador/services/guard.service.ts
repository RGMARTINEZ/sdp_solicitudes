import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router } from '@angular/router';
import { TokenAdminService } from '../../integrador/services/token-admin.service';

@Injectable()

export class GuardService implements CanActivate {

  constructor(
    public tokenAdmin: TokenAdminService,
    public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (!this.tokenAdmin.isAuthenticate()) {
      this.router.navigate(['']);
      return false;
    }
    return true;
  }

}
