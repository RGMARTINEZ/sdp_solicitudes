import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()

export class ConfiguracionService {

  public user: any;
  subShowLoading: Subject<boolean> = new Subject();
  subShowLoadingRequest: Subject<boolean> = new Subject();

  constructor() {
      this.user = {
        profile: '',
        name: '',
        job: '',
        picture: ''
    };

  }

  getUserConfig(name) {
      return name ? this.user[name] : this.user;
  }

  setUserConfig(name, value) {
      if (typeof this.user[name] !== 'undefined') {
          this.user[name] = value;
      }
  }

  showLoading(flat) {
    this.subShowLoading.next(flat);
  }

  showLoadingRequest(flat2) {
    this.subShowLoadingRequest.next(flat2);
  }

}
