import { TestBed, inject } from '@angular/core/testing';

import { TokenAdminService } from './token-admin.service';

describe('TokenAdminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TokenAdminService]
    });
  });

  it('should be created', inject([TokenAdminService], (service: TokenAdminService) => {
    expect(service).toBeTruthy();
  }));
});
