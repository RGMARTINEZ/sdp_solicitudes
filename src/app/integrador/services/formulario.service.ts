import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { LOCALIDADES } from './localidades-data';

let authorizationData = 'Basic ' + btoa(environment.API_KEY + ':' + environment.API_SECRET);


@Injectable()

export class FormularioService extends HttpService {

 

  constructor(http: HttpClient) {
    super(http);
    this.END_POINT = environment.END_POINT_SSO;
   

  }


  makeCapitalPopup(data: any): string {

    let arrayString = '';
    for (const c of data) {
      arrayString = `<div><b>Operador:</b> &nbsp;${ c.nombre }</div> <br>` + arrayString;
    }
    return `` + arrayString;
  }


  makeCapitalPopup_operators(data: any): string {
    return `` +
        `<div><b>Dirección Normalizada:</b> &nbsp;${ data.address }</div> <br>` +     
        `<div><b>Localidad:</b> &nbsp;${ data.admin4 }</div> <br>` +     
        `<div><b>Barrio:</b> &nbsp;${ data.admin5 }</div> <br>`+           
        `<div><b>Complemento:</b> &nbsp;${ data.commonName }</div> <br>`   
  }


  makeCapitalPopup_emergencias(data: any): string {
    return `` +
        `<div><b>Identificación:</b> &nbsp;${ data.id }</div> <br>` +     
        `<div><b>Tipo de Emergencia:</b> &nbsp;${ data.tipo }</div> <br>` +     
        `<div><b>Estado:</b> &nbsp;${ data.estado }</div> <br>`+           
        `<div><b>Ciudad:</b> &nbsp;${ data.ciudad }</div> <br>` +     
        `<div><b>Direccion:</b> &nbsp;${ data.direccion }</div> <br>`+
        `<div><b>Nombre Solicitante:</b> &nbsp;${ data.nombreSolicitante }</div> <br>`+           
        `<div><b>Telefono de Contacto:</b> &nbsp;${ data.telefonoSolicitante }</div> <br>` +     
        `<div><b>Fecha y hora de recepción:</b> &nbsp;${ data.createdAt }</div> <br>`    
  }


  public getDireccion(id) {
    this.END_POINT = ''

    return this.get(`https://api.lupap.co/v2/co/bogota?a=${id}&key=9321aa7556f95b76c3d69514bab52c9d3290da13`);
  }


  public getLocalidades() {
    this.END_POINT = environment.END_POINT_SSO;
    //return this.get('catalogo/municipios');
    return LOCALIDADES;
  }

  public getBarrios() {
    this.END_POINT = environment.END_POINT_SSO;
    //return this.get('catalogo/municipios');
    return this.get('assets/data/barrios.json');
  }

  public getUpz() {
    this.END_POINT = environment.END_POINT_SSO;
    //return this.get('catalogo/municipios');
    return this.get('assets/data/upzs.json');
  }


  public enviarAnalisisGrupoFamiliar(payLoad) {
    this.END_POINT = environment.END_POINT_SSO;
    return this.post('customer/validateGrupoFamiliar', payLoad );

  }

  public enviarAnalisisSolicitante(payLoad) {
    this.END_POINT = environment.END_POINT_SSO;
    return this.post('customer/validateSolicitante', payLoad );

  }

  public enviarRegistroSolicitante(payLoad) {
    this.END_POINT = environment.END_POINT_SSO;
    return this.post(`customer/saveSolicitante`, payLoad);

  }

  public enviarRegistroGrupoFamiliar(payLoad) {
    this.END_POINT = environment.END_POINT_SSO;
    return this.post(`customer/saveGrupoFamiliar`, payLoad);

  }

  public consultarDocumento(payLoad) {
    this.END_POINT = 'http://10.8.100.154:8787/';
    return this.get(`api/rnec/${payLoad}`);

  }


  public consultarDocumentoTemp(payLoad) {
    this.END_POINT = 'https://b553f686-dd5d-4ed4-9d59-af7ab5138554.mock.pstmn.io/';
    return this.get(`api/rnec/22373864`);

  }




}




