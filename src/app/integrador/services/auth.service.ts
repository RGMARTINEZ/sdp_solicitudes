import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()

export class AuthService extends HttpService {

  public logged: boolean;

  constructor(http: HttpClient) {
    super(http);
    this.END_POINT = environment.END_POINT_SSO;
  }

  public login(data) {
    this.END_POINT = environment.END_POINT_SSO;
    return this.post('autenticacion/login', data);
  }

  public validarContrasena(data) {
    this.END_POINT = environment.END_POINT_SSO;
    return this.post('autenticacion/valideTemporal', data);
  }

  public recover(data) {
    this.END_POINT = environment.END_POINT_SSO;
    return this.post(`/dane/api/v1/cambiarPassword?email=${data.email}`, '');
  }

  public cambioContrasena(data) {
    this.END_POINT = environment.END_POINT_SSO;
    return this.post('autenticacion/cambioContrasena', data);
  }

  public refreshToken(data) {
    this.END_POINT = environment.END_POINT_SSO;
    return this.post('Login.svc/refreshToken', data);
  }

}
