import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { environment } from '../../../environments/environment';

let authorizationData = 'Basic ' + btoa(environment.API_KEY + ':' + environment.API_SECRET);


@Injectable()

export class HttpService implements OnInit {

  private headers;
  private options;
  public END_POINT = '';

  constructor(private http: HttpClient) {

  }

  ngOnInit() {}


  public get(operation: string) {
    return this.http.get(this.END_POINT + operation).map(this.adminData).catch(this.adminError);
  }



  public post(operation: string, payload: any) {
    return this.http.post(this.END_POINT + operation, payload).map(this.adminData).catch(this.adminError);
  }

  public put(operation: string, payload: any) {
    return this.http.put(this.END_POINT + operation, payload).map(this.adminData).catch(this.adminError);
  }

  public delete(operation: string) {
    return this.http.delete(this.END_POINT + operation).map(this.adminData).catch(this.adminError);
  }

  private adminData(response: any) {
    const temp =  response;
    return temp || { };
  }

  private adminError(error: Response | any) {

    let mensajeError: string;
    if (error instanceof Response) {
      const body: any = error.json() || '';
      const err = body.error || JSON.stringify(body);
      mensajeError = `${error.status} - ${error.statusText || ''} ${err}`;
      //console.log(mensajeError, 'ERRORES')
    } else {
       mensajeError = error.error ? error.error : error.toString();
       //console.log(mensajeError, 'ERRORES1')
    }
    return Observable.throwError(mensajeError);
  }

  }
