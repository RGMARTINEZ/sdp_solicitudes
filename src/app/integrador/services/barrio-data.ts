export const BARRIOS = [
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008401",
        "NOMBRE_BARRIO": "LA CALLEJA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008402",
        "NOMBRE_BARRIO": "COUNTRY CLUB"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008403",
        "NOMBRE_BARRIO": "LA CAROLINA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008404",
        "NOMBRE_BARRIO": "BELLA SUIZA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008405",
        "NOMBRE_BARRIO": "GINEBRA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008406",
        "NOMBRE_BARRIO": "SAN GABRIEL NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008407",
        "NOMBRE_BARRIO": "USAQUEN"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008408",
        "NOMBRE_BARRIO": "SANTA ANA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008409",
        "NOMBRE_BARRIO": "ESCUELA DE CABALLERIA I"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008410",
        "NOMBRE_BARRIO": "ESCUELA DE INFANTERIA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008411",
        "NOMBRE_BARRIO": "RINCON DEL CHICO"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008412",
        "NOMBRE_BARRIO": "SANTA BIBIANA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008413",
        "NOMBRE_BARRIO": "SANTA BARBARA ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008414",
        "NOMBRE_BARRIO": "SANTA ANA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008415",
        "NOMBRE_BARRIO": "SANTA BARBARA CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008416",
        "NOMBRE_BARRIO": "MOLINOS NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008417",
        "NOMBRE_BARRIO": "SANTA BARBARA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008418",
        "NOMBRE_BARRIO": "SAN PATRICIO"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008419",
        "NOMBRE_BARRIO": "ESCUELA DE CABALLERIA II"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008420",
        "NOMBRE_BARRIO": "PARAMO URBANO I"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008424",
        "NOMBRE_BARRIO": "SEGUNDO CONTADOR"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008425",
        "NOMBRE_BARRIO": "GINEBRA II"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008426",
        "NOMBRE_BARRIO": "SAN GABRIEL NORTE II"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008501",
        "NOMBRE_BARRIO": "EL TOBERIN"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008502",
        "NOMBRE_BARRIO": "LA PRADERA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008503",
        "NOMBRE_BARRIO": "SANTA TERESA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008504",
        "NOMBRE_BARRIO": "LA CITA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008505",
        "NOMBRE_BARRIO": "SAN CRISTOBAL NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008506",
        "NOMBRE_BARRIO": "BARRANCAS NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008507",
        "NOMBRE_BARRIO": "BARRANCAS"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008508",
        "NOMBRE_BARRIO": "CEDRO SALAZAR"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008509",
        "NOMBRE_BARRIO": "LOS CEDROS ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008510",
        "NOMBRE_BARRIO": "ACACIAS USAQUEN"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008511",
        "NOMBRE_BARRIO": "CEDRO NARVAEZ"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008512",
        "NOMBRE_BARRIO": "CEDRITOS"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008513",
        "NOMBRE_BARRIO": "LISBOA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008514",
        "NOMBRE_BARRIO": "EL CONTADOR"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008515",
        "NOMBRE_BARRIO": "LOS CEDROS"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008516",
        "NOMBRE_BARRIO": "LAS ORQUIDEAS"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008517",
        "NOMBRE_BARRIO": "LA LIBERIA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008518",
        "NOMBRE_BARRIO": "CAOBOS SALAZAR"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008519",
        "NOMBRE_BARRIO": "ESTRELLA DEL NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008520",
        "NOMBRE_BARRIO": "BOSQUE DE PINOS"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008521",
        "NOMBRE_BARRIO": "TIBABITA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008522",
        "NOMBRE_BARRIO": "EL ROCIO NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008523",
        "NOMBRE_BARRIO": "SAN ANTONIO NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008524",
        "NOMBRE_BARRIO": "LA GRANJA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008525",
        "NOMBRE_BARRIO": "LA URIBE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008526",
        "NOMBRE_BARRIO": "EL VERVENAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008527",
        "NOMBRE_BARRIO": "EL CEREZO"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008528",
        "NOMBRE_BARRIO": "SAN ANTONIO NOROCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008529",
        "NOMBRE_BARRIO": "SAN JOSE DE USAQUEN"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008530",
        "NOMBRE_BARRIO": "LAS MARGARITAS"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008532",
        "NOMBRE_BARRIO": "EL REDIL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008533",
        "NOMBRE_BARRIO": "HORIZONTES NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008535",
        "NOMBRE_BARRIO": "BUENAVISTA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008536",
        "NOMBRE_BARRIO": "BOSQUE DE PINOS I"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008537",
        "NOMBRE_BARRIO": "VERBENAL SAN ANTONIO"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008538",
        "NOMBRE_BARRIO": "BOSQUE DE PINOS III"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008539",
        "NOMBRE_BARRIO": "TORCA I"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008540",
        "NOMBRE_BARRIO": "LA ESTRELLITA I"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008541",
        "NOMBRE_BARRIO": "TORCA II"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008542",
        "NOMBRE_BARRIO": "TIBABITA I"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008543",
        "NOMBRE_BARRIO": "SANTA CECILIA PUENTE NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008544",
        "NOMBRE_BARRIO": "CANAIMA"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008545",
        "NOMBRE_BARRIO": "MIRADOR DEL NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "008547",
        "NOMBRE_BARRIO": "LA ESTRELLITA III"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "108101",
        "NOMBRE_BARRIO": "TORCA RURAL I"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "108102",
        "NOMBRE_BARRIO": "TIBABITA RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "108103",
        "NOMBRE_BARRIO": "BARRANCAS ORIENTAL RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "108104",
        "NOMBRE_BARRIO": "PARAMO"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "108110",
        "NOMBRE_BARRIO": "TORCA RURAL II"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "108111",
        "NOMBRE_BARRIO": "TIBABITA RURAL I"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "108112",
        "NOMBRE_BARRIO": "PARAMO II RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "108113",
        "NOMBRE_BARRIO": "BOSQUE DE PINOS III RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "108114",
        "NOMBRE_BARRIO": "PARAMO III RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "208103",
        "NOMBRE_BARRIO": "BARRANCAS ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "1",
        "NOMBRE_LOCALIDAD": "USAQUEN",
        "CODIGO_BARRIO": "208203",
        "NOMBRE_BARRIO": "LA ESTRELLITA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005404",
        "NOMBRE_BARRIO": "LAS FERIAS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005405",
        "NOMBRE_BARRIO": "LAS FERIAS OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005406",
        "NOMBRE_BARRIO": "BONANZA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005501",
        "NOMBRE_BARRIO": "PALO BLANCO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005502",
        "NOMBRE_BARRIO": "LA ESTRADA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005503",
        "NOMBRE_BARRIO": "BELLAVISTA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005504",
        "NOMBRE_BARRIO": "LA ESTRADITA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005505",
        "NOMBRE_BARRIO": "BOSQUE POPULAR"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005506",
        "NOMBRE_BARRIO": "JARDIN BOTANICO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005507",
        "NOMBRE_BARRIO": "NORMANDIA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005508",
        "NOMBRE_BARRIO": "LA CABANA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005509",
        "NOMBRE_BARRIO": "SAN JOAQUIN"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005510",
        "NOMBRE_BARRIO": "EL LAUREL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005601",
        "NOMBRE_BARRIO": "EL MINUTO DE DIOS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005602",
        "NOMBRE_BARRIO": "SANTA MARIA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005603",
        "NOMBRE_BARRIO": "BOYACA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005604",
        "NOMBRE_BARRIO": "EL REAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005605",
        "NOMBRE_BARRIO": "EL ENCANTO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005606",
        "NOMBRE_BARRIO": "NORMANDIA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005607",
        "NOMBRE_BARRIO": "SAN IGNACIO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005608",
        "NOMBRE_BARRIO": "SANTA HELENITA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005609",
        "NOMBRE_BARRIO": "TABORA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005610",
        "NOMBRE_BARRIO": "LA GRANJA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005611",
        "NOMBRE_BARRIO": "AUTOPISTA MEDELLIN"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005612",
        "NOMBRE_BARRIO": "PARIS GAITAN"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005613",
        "NOMBRE_BARRIO": "LA SERENA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005614",
        "NOMBRE_BARRIO": "VILLA LUZ"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005615",
        "NOMBRE_BARRIO": "LA SOLEDAD NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005616",
        "NOMBRE_BARRIO": "LOS CEREZOS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005617",
        "NOMBRE_BARRIO": "PRIMAVERA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005619",
        "NOMBRE_BARRIO": "PARIS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005620",
        "NOMBRE_BARRIO": "FLORENCIA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005621",
        "NOMBRE_BARRIO": "FLORIDA BLANCA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005622",
        "NOMBRE_BARRIO": "BOLIVIA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005623",
        "NOMBRE_BARRIO": "LOS ANGELES"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005625",
        "NOMBRE_BARRIO": "LOS ALAMOS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005626",
        "NOMBRE_BARRIO": "LOS ALAMOS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005627",
        "NOMBRE_BARRIO": "GARCES NAVAS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005628",
        "NOMBRE_BARRIO": "GARCES NAVAS ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005629",
        "NOMBRE_BARRIO": "EL CEDRO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005630",
        "NOMBRE_BARRIO": "CIUDAD BACHUE"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005631",
        "NOMBRE_BARRIO": "CIUDAD BACHUE I ETAPA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005632",
        "NOMBRE_BARRIO": "BOCHICA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005634",
        "NOMBRE_BARRIO": "EL CORTIJO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005635",
        "NOMBRE_BARRIO": "EL MADRIGAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005636",
        "NOMBRE_BARRIO": "ENGATIVA ZONA URBANA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005637",
        "NOMBRE_BARRIO": "VILLA GLADYS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005638",
        "NOMBRE_BARRIO": "SANTA CECILIA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005639",
        "NOMBRE_BARRIO": "QUIRIGUA ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005640",
        "NOMBRE_BARRIO": "BOLIVIA ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005641",
        "NOMBRE_BARRIO": "SANTA MONICA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005643",
        "NOMBRE_BARRIO": "ALAMOS"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005644",
        "NOMBRE_BARRIO": "BOCHICA II"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005647",
        "NOMBRE_BARRIO": "VILLAS DE GRANADA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005648",
        "NOMBRE_BARRIO": "VILLA AMALIA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005649",
        "NOMBRE_BARRIO": "VILLAS DE GRANADA I"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005650",
        "NOMBRE_BARRIO": "CIUDADELA COLSUBSIDIO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005652",
        "NOMBRE_BARRIO": "EL MUELLE"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005654",
        "NOMBRE_BARRIO": "GRAN GRANADA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005655",
        "NOMBRE_BARRIO": "GARCES NAVAS SUR"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005657",
        "NOMBRE_BARRIO": "SABANA DEL DORADO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005658",
        "NOMBRE_BARRIO": "VILLA SAGRARIO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005660",
        "NOMBRE_BARRIO": "SAN ANTONIO URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005661",
        "NOMBRE_BARRIO": "LUIS CARLOS GALAN"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005662",
        "NOMBRE_BARRIO": "EL CEDRO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005663",
        "NOMBRE_BARRIO": "EL DORADO INDUSTRIAL"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005664",
        "NOMBRE_BARRIO": "EL DORADO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005665",
        "NOMBRE_BARRIO": "MARANDU"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005666",
        "NOMBRE_BARRIO": "LA FAENA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005667",
        "NOMBRE_BARRIO": "CENTRO ENGATIVA II"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005668",
        "NOMBRE_BARRIO": "VILLAS DE ALCALA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005669",
        "NOMBRE_BARRIO": "EL GACO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005670",
        "NOMBRE_BARRIO": "LA RIVIERA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005675",
        "NOMBRE_BARRIO": "BOLIVIA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005679",
        "NOMBRE_BARRIO": "SAN ANTONIO ENGATIVA"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005680",
        "NOMBRE_BARRIO": "VILLA DEL MAR"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005682",
        "NOMBRE_BARRIO": "CIUDAD BACHUE II"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005683",
        "NOMBRE_BARRIO": "QUIRIGUA I"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "005684",
        "NOMBRE_BARRIO": "QUIRIGUA II"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "106201",
        "NOMBRE_BARRIO": "EL PANTANO"
    },
    {
        "CODIGO_LOCALIDAD": "10",
        "NOMBRE_LOCALIDAD": "ENGATIVA",
        "CODIGO_BARRIO": "106301",
        "NOMBRE_BARRIO": "ENGATIVA EL DORADO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "005401",
        "NOMBRE_BARRIO": "POTOSI"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "005402",
        "NOMBRE_BARRIO": "SANTA ROSA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "005403",
        "NOMBRE_BARRIO": "JULIO FLOREZ"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009101",
        "NOMBRE_BARRIO": "GRANADA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009102",
        "NOMBRE_BARRIO": "BRITALIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009103",
        "NOMBRE_BARRIO": "CANTAGALLO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009104",
        "NOMBRE_BARRIO": "VICTORIA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009105",
        "NOMBRE_BARRIO": "PRADO PINZON"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009106",
        "NOMBRE_BARRIO": "SAN JOSE DEL PRADO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009107",
        "NOMBRE_BARRIO": "PRADO VERANIEGO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009108",
        "NOMBRE_BARRIO": "CIUDAD JARDIN NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009109",
        "NOMBRE_BARRIO": "NIZA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009110",
        "NOMBRE_BARRIO": "MAZUREN"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009111",
        "NOMBRE_BARRIO": "MONACO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009112",
        "NOMBRE_BARRIO": "NIZA SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009113",
        "NOMBRE_BARRIO": "SAN JOSE DE BAVARIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009114",
        "NOMBRE_BARRIO": "GILMAR"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009115",
        "NOMBRE_BARRIO": "IBERIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009116",
        "NOMBRE_BARRIO": "PRADO VERANIEGO NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009117",
        "NOMBRE_BARRIO": "PRADO VERANIEGO SUR"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009118",
        "NOMBRE_BARRIO": "BATAN"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009119",
        "NOMBRE_BARRIO": "ATENAS"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009120",
        "NOMBRE_BARRIO": "NUEVA ZELANDIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009121",
        "NOMBRE_BARRIO": "CLUB DE LOS LAGARTOS"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009122",
        "NOMBRE_BARRIO": "LAS VILLAS"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009123",
        "NOMBRE_BARRIO": "NIZA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009124",
        "NOMBRE_BARRIO": "PUENTE LARGO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009125",
        "NOMBRE_BARRIO": "PASADENA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009126",
        "NOMBRE_BARRIO": "ESTORIL"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009127",
        "NOMBRE_BARRIO": "ANDES NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009128",
        "NOMBRE_BARRIO": "EL PLAN"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009129",
        "NOMBRE_BARRIO": "VILLA DEL PRADO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009130",
        "NOMBRE_BARRIO": "CANODROMO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009131",
        "NOMBRE_BARRIO": "SANTA HELENA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009132",
        "NOMBRE_BARRIO": "ESCUELA DE CARABINEROS"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009133",
        "NOMBRE_BARRIO": "MIRANDELA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009134",
        "NOMBRE_BARRIO": "SAN JOSE V SECTOR"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009135",
        "NOMBRE_BARRIO": "PORTALES DEL NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009136",
        "NOMBRE_BARRIO": "CASABLANCA SUBA URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009138",
        "NOMBRE_BARRIO": "CASABLANCA SUBA URBANO I"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009139",
        "NOMBRE_BARRIO": "CASABLANCA SUBA URBANO II"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009140",
        "NOMBRE_BARRIO": "NUESTRA SENORA DEL ROSARIO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009141",
        "NOMBRE_BARRIO": "LA CANDELARIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009142",
        "NOMBRE_BARRIO": "CONEJERA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009201",
        "NOMBRE_BARRIO": "LOS NARANJOS"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009202",
        "NOMBRE_BARRIO": "EL RINCON"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009203",
        "NOMBRE_BARRIO": "PUERTA DEL SOL"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009204",
        "NOMBRE_BARRIO": "EL RINCON NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009205",
        "NOMBRE_BARRIO": "AURES"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009206",
        "NOMBRE_BARRIO": "AURES II"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009207",
        "NOMBRE_BARRIO": "VILLA ELISA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009208",
        "NOMBRE_BARRIO": "COSTA AZUL"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009209",
        "NOMBRE_BARRIO": "VILLA MARIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009210",
        "NOMBRE_BARRIO": "LAS FLORES"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009211",
        "NOMBRE_BARRIO": "EL POA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009212",
        "NOMBRE_BARRIO": "SUBA URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009213",
        "NOMBRE_BARRIO": "EL PINO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009214",
        "NOMBRE_BARRIO": "TUNA ALTA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009215",
        "NOMBRE_BARRIO": "CASABLANCA SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009216",
        "NOMBRE_BARRIO": "LA GAITANA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009217",
        "NOMBRE_BARRIO": "RINCON DE SANTA INES"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009218",
        "NOMBRE_BARRIO": "ALMIRANTE COLON"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009219",
        "NOMBRE_BARRIO": "CAMPANELLA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009220",
        "NOMBRE_BARRIO": "LAGO DE SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009221",
        "NOMBRE_BARRIO": "LA CHUCUA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009222",
        "NOMBRE_BARRIO": "ALTOS DE CHOZICA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009223",
        "NOMBRE_BARRIO": "TTES DE COLOMBIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009224",
        "NOMBRE_BARRIO": "NUEVA TIBABUYES"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009225",
        "NOMBRE_BARRIO": "TIBABUYES UNIVERSAL"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009226",
        "NOMBRE_BARRIO": "SALITRE SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009227",
        "NOMBRE_BARRIO": "LECH WALESA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009228",
        "NOMBRE_BARRIO": "LOMBARDIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009229",
        "NOMBRE_BARRIO": "TOSCANA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009230",
        "NOMBRE_BARRIO": "SAN CAYETANO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009231",
        "NOMBRE_BARRIO": "SUBA CERROS"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009232",
        "NOMBRE_BARRIO": "SABANA DE TIBABUYES"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009233",
        "NOMBRE_BARRIO": "TIBABUYES"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009234",
        "NOMBRE_BARRIO": "SABANA DE TIBABUYES NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009235",
        "NOMBRE_BARRIO": "CIUDAD HUNZA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009236",
        "NOMBRE_BARRIO": "LA CAROLINA DE SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009237",
        "NOMBRE_BARRIO": "RINCON DE SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009238",
        "NOMBRE_BARRIO": "SANTA TERESA DE SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009239",
        "NOMBRE_BARRIO": "LAS MERCEDES SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009240",
        "NOMBRE_BARRIO": "POTRERILLO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009241",
        "NOMBRE_BARRIO": "BOSQUES DE SAN JORGE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009242",
        "NOMBRE_BARRIO": "TUNA BAJA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009243",
        "NOMBRE_BARRIO": "LAS MERCEDES I"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009244",
        "NOMBRE_BARRIO": "VILLA HERMOSA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009245",
        "NOMBRE_BARRIO": "PINOS DE LOMBARDIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009246",
        "NOMBRE_BARRIO": "DELMONTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009247",
        "NOMBRE_BARRIO": "IRAGUA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009248",
        "NOMBRE_BARRIO": "TIBABUYES II"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009249",
        "NOMBRE_BARRIO": "VEREDA SUBA NARANJOS"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009250",
        "NOMBRE_BARRIO": "TIBABUYES OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009251",
        "NOMBRE_BARRIO": "SAN CARLOS DE SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009252",
        "NOMBRE_BARRIO": "BILBAO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009253",
        "NOMBRE_BARRIO": "SAN PEDRO"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009254",
        "NOMBRE_BARRIO": "LISBOA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009255",
        "NOMBRE_BARRIO": "SANTA RITA DE SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009256",
        "NOMBRE_BARRIO": "SANTA CECILIA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009257",
        "NOMBRE_BARRIO": "TIBABUYES"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009258",
        "NOMBRE_BARRIO": "CASA BLANCA SUBA I"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009259",
        "NOMBRE_BARRIO": "BERLIN"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009260",
        "NOMBRE_BARRIO": "ALTOS DE SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009261",
        "NOMBRE_BARRIO": "TUNA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009262",
        "NOMBRE_BARRIO": "LA GAITANA ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009263",
        "NOMBRE_BARRIO": "VILLA ALCAZAR"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009265",
        "NOMBRE_BARRIO": "VEREDA SUBA CERROS II"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009266",
        "NOMBRE_BARRIO": "RINCON ALTAMAR"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "009267",
        "NOMBRE_BARRIO": "VILLA MARIA I"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "107101",
        "NOMBRE_BARRIO": "GUAYMARAL"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "107102",
        "NOMBRE_BARRIO": "CASABLANCA SUBA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "107106",
        "NOMBRE_BARRIO": "TUNA RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "107107",
        "NOMBRE_BARRIO": "LAS MERCEDES SUBA RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "107109",
        "NOMBRE_BARRIO": "BARAJAS NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "107110",
        "NOMBRE_BARRIO": "CASABLANCA SUBA I"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "107111",
        "NOMBRE_BARRIO": "CASABLANCA SUBA II"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "107112",
        "NOMBRE_BARRIO": "LA LOMITA"
    },
    {
        "CODIGO_LOCALIDAD": "11",
        "NOMBRE_LOCALIDAD": "SUBA",
        "CODIGO_BARRIO": "207801",
        "NOMBRE_BARRIO": "SANTA CECILIA"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005101",
        "NOMBRE_BARRIO": "JOSE JOAQUIN VARGAS"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005102",
        "NOMBRE_BARRIO": "POPULAR MODELO"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005103",
        "NOMBRE_BARRIO": "SAN MIGUEL"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005104",
        "NOMBRE_BARRIO": "EL ROSARIO"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005112",
        "NOMBRE_BARRIO": "PARQUE DISTRITAL SALITRE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005115",
        "NOMBRE_BARRIO": "PARQUE POPULAR SALITRE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005201",
        "NOMBRE_BARRIO": "JORGE ELIECER GAITAN"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005202",
        "NOMBRE_BARRIO": "DOCE DE OCTUBRE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005203",
        "NOMBRE_BARRIO": "SAN FERNANDO"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005204",
        "NOMBRE_BARRIO": "SAN FERNANDO OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005205",
        "NOMBRE_BARRIO": "SIMON BOLIVAR"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005206",
        "NOMBRE_BARRIO": "LA LIBERTAD"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005207",
        "NOMBRE_BARRIO": "METROPOLIS"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005304",
        "NOMBRE_BARRIO": "LA CASTELLANA"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005305",
        "NOMBRE_BARRIO": "LA PATRIA"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005306",
        "NOMBRE_BARRIO": "ENTRERIOS"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005307",
        "NOMBRE_BARRIO": "LOS ANDES"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005308",
        "NOMBRE_BARRIO": "RIONEGRO"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "005310",
        "NOMBRE_BARRIO": "ESCUELA MILITAR"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007301",
        "NOMBRE_BARRIO": "LA MERCED NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007302",
        "NOMBRE_BARRIO": "ALCAZARES"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007303",
        "NOMBRE_BARRIO": "COLOMBIA"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007304",
        "NOMBRE_BARRIO": "CONCEPCION NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007305",
        "NOMBRE_BARRIO": "LA ESPERANZA"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007306",
        "NOMBRE_BARRIO": "BAQUERO"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007307",
        "NOMBRE_BARRIO": "MUEQUETA"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007308",
        "NOMBRE_BARRIO": "QUINTA MUTIS"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007309",
        "NOMBRE_BARRIO": "BENJAMIN HERRERA"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007310",
        "NOMBRE_BARRIO": "LA PAZ"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007311",
        "NOMBRE_BARRIO": "SIETE DE AGOSTO"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007312",
        "NOMBRE_BARRIO": "RAFAEL URIBE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007401",
        "NOMBRE_BARRIO": "POLO CLUB"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007402",
        "NOMBRE_BARRIO": "JUAN XXIII"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007403",
        "NOMBRE_BARRIO": "SAN FELIPE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007404",
        "NOMBRE_BARRIO": "ALCAZARES NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007405",
        "NOMBRE_BARRIO": "ONCE DE NOVIEMBRE"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007406",
        "NOMBRE_BARRIO": "SANTA SOFIA"
    },
    {
        "CODIGO_LOCALIDAD": "12",
        "NOMBRE_LOCALIDAD": "BARRIOS UNIDOS",
        "CODIGO_BARRIO": "007407",
        "NOMBRE_BARRIO": "LA AURORA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005105",
        "NOMBRE_BARRIO": "CAMPIN OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005106",
        "NOMBRE_BARRIO": "NICOLAS DE FEDERMAN"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005107",
        "NOMBRE_BARRIO": "ACEVEDO TEJADA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005108",
        "NOMBRE_BARRIO": "CIUDAD UNIVERSITARIA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005109",
        "NOMBRE_BARRIO": "CENTRO ADMINISTRATIVO OCC."
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005110",
        "NOMBRE_BARRIO": "EL SALITRE"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005111",
        "NOMBRE_BARRIO": "LA ESMERALDA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005113",
        "NOMBRE_BARRIO": "PAULO VI"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005114",
        "NOMBRE_BARRIO": "CAMPO EUCARISTICO"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005116",
        "NOMBRE_BARRIO": "PABLO VI NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "005117",
        "NOMBRE_BARRIO": "RAFAEL NUNEZ"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "006201",
        "NOMBRE_BARRIO": "EL RECUERDO"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "006202",
        "NOMBRE_BARRIO": "GRAN AMERICA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "006209",
        "NOMBRE_BARRIO": "QUINTA PAREDES"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "006210",
        "NOMBRE_BARRIO": "CENTRO NARINO"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "006216",
        "NOMBRE_BARRIO": "CIUDAD SALITRE NOR-ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "006217",
        "NOMBRE_BARRIO": "CIUDAD SALITRE SUR-ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007101",
        "NOMBRE_BARRIO": "LA SOLEDAD"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007102",
        "NOMBRE_BARRIO": "SANTA TERESITA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007103",
        "NOMBRE_BARRIO": "LA MAGDALENA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007104",
        "NOMBRE_BARRIO": "TEUSAQUILLO"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007105",
        "NOMBRE_BARRIO": "ARMENIA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007106",
        "NOMBRE_BARRIO": "ESTRELLA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007107",
        "NOMBRE_BARRIO": "LAS AMERICAS"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007201",
        "NOMBRE_BARRIO": "CAMPIN"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007202",
        "NOMBRE_BARRIO": "SAN LUIS"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007203",
        "NOMBRE_BARRIO": "CHAPINERO OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007204",
        "NOMBRE_BARRIO": "QUESADA"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007205",
        "NOMBRE_BARRIO": "PALERMO"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007206",
        "NOMBRE_BARRIO": "BELALCAZAR"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007207",
        "NOMBRE_BARRIO": "GALERIAS"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007208",
        "NOMBRE_BARRIO": "BANCO CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "13",
        "NOMBRE_LOCALIDAD": "TEUSAQUILLO",
        "CODIGO_BARRIO": "007209",
        "NOMBRE_BARRIO": "ALFONSO LOPEZ"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004101",
        "NOMBRE_BARRIO": "RICAURTE"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004102",
        "NOMBRE_BARRIO": "LA SABANA"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004103",
        "NOMBRE_BARRIO": "VOTO NACIONAL"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004104",
        "NOMBRE_BARRIO": "LA ESTANZUELA"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004105",
        "NOMBRE_BARRIO": "EDUARDO SANTOS"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004106",
        "NOMBRE_BARRIO": "EL VERGEL"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004107",
        "NOMBRE_BARRIO": "SANTA ISABEL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004108",
        "NOMBRE_BARRIO": "SANTA ISABEL"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004109",
        "NOMBRE_BARRIO": "VERAGUAS"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004110",
        "NOMBRE_BARRIO": "LA PEPITA"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "004111",
        "NOMBRE_BARRIO": "EL PROGRESO"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "006101",
        "NOMBRE_BARRIO": "FLORIDA"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "006103",
        "NOMBRE_BARRIO": "SANTA FE"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "006104",
        "NOMBRE_BARRIO": "LA FAVORITA"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "006105",
        "NOMBRE_BARRIO": "SAN VICTORINO"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "006106",
        "NOMBRE_BARRIO": "EL LISTON"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "006107",
        "NOMBRE_BARRIO": "PALOQUEMAO"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "006108",
        "NOMBRE_BARRIO": "SAMPER MENDOZA"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "006109",
        "NOMBRE_BARRIO": "COLSEGUROS"
    },
    {
        "CODIGO_LOCALIDAD": "14",
        "NOMBRE_LOCALIDAD": "LOS MARTIRES",
        "CODIGO_BARRIO": "006110",
        "NOMBRE_BARRIO": "USATAMA"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "001201",
        "NOMBRE_BARRIO": "SEVILLA"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "001202",
        "NOMBRE_BARRIO": "CIUDAD BERNA"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "001203",
        "NOMBRE_BARRIO": "CARACAS"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "001204",
        "NOMBRE_BARRIO": "CIUDAD JARDIN SUR"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "001208",
        "NOMBRE_BARRIO": "LA HORTUA"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "001211",
        "NOMBRE_BARRIO": "POLICARPA"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "002101",
        "NOMBRE_BARRIO": "LA FRAGUITA"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "002102",
        "NOMBRE_BARRIO": "SAN ANTONIO"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "002103",
        "NOMBRE_BARRIO": "RESTREPO"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "002104",
        "NOMBRE_BARRIO": "RESTREPO OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "002105",
        "NOMBRE_BARRIO": "SANTANDER"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "002106",
        "NOMBRE_BARRIO": "SENA"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "002107",
        "NOMBRE_BARRIO": "LA FRAGUA"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "002301",
        "NOMBRE_BARRIO": "EDUARDO FREY"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "002302",
        "NOMBRE_BARRIO": "SANTANDER SUR"
    },
    {
        "CODIGO_LOCALIDAD": "15",
        "NOMBRE_LOCALIDAD": "ANTONIO NARIÑO",
        "CODIGO_BARRIO": "002309",
        "NOMBRE_BARRIO": "VILLA MAYOR ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004201",
        "NOMBRE_BARRIO": "PENSILVANIA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004202",
        "NOMBRE_BARRIO": "COMUNEROS"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004203",
        "NOMBRE_BARRIO": "LA ASUNCION"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004204",
        "NOMBRE_BARRIO": "MONTES"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004205",
        "NOMBRE_BARRIO": "PRIMAVERA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004206",
        "NOMBRE_BARRIO": "SAN FRANCISCO"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004207",
        "NOMBRE_BARRIO": "GORGONZOLA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004208",
        "NOMBRE_BARRIO": "LOS EJIDOS"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004209",
        "NOMBRE_BARRIO": "SANTA MATILDE"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004210",
        "NOMBRE_BARRIO": "JORGE GAITAN CORTES"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004211",
        "NOMBRE_BARRIO": "BOCHICA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004212",
        "NOMBRE_BARRIO": "TIBANA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004301",
        "NOMBRE_BARRIO": "SAN RAFAEL INDUSTRIAL"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004302",
        "NOMBRE_BARRIO": "SAN RAFAEL"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004303",
        "NOMBRE_BARRIO": "BARCELONA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004304",
        "NOMBRE_BARRIO": "GALAN"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004305",
        "NOMBRE_BARRIO": "LA PRADERA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004306",
        "NOMBRE_BARRIO": "LA TRINIDAD"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004307",
        "NOMBRE_BARRIO": "SAN GABRIEL"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004308",
        "NOMBRE_BARRIO": "COLON"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004309",
        "NOMBRE_BARRIO": "LA CAMELIA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004310",
        "NOMBRE_BARRIO": "PROVIVIENDA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004317",
        "NOMBRE_BARRIO": "LA CAMELIA II"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004401",
        "NOMBRE_BARRIO": "SAN EUSEBIO"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004402",
        "NOMBRE_BARRIO": "REMANSO"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004403",
        "NOMBRE_BARRIO": "AUTOPISTA MUZU"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004404",
        "NOMBRE_BARRIO": "AUTOPISTA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004405",
        "NOMBRE_BARRIO": "OSPINA PEREZ SUR"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004406",
        "NOMBRE_BARRIO": "OSPINA PEREZ"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004407",
        "NOMBRE_BARRIO": "ALCALA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004408",
        "NOMBRE_BARRIO": "TEJAR"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004409",
        "NOMBRE_BARRIO": "ALQUERIA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004412",
        "NOMBRE_BARRIO": "REMANSO SUR"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "004413",
        "NOMBRE_BARRIO": "AUTOPISTA MUZU ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "006203",
        "NOMBRE_BARRIO": "LA FLORIDA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "006204",
        "NOMBRE_BARRIO": "ESTACION CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "006205",
        "NOMBRE_BARRIO": "INDUSTRIAL CENTENARIO"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "006206",
        "NOMBRE_BARRIO": "EL EJIDO"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "006207",
        "NOMBRE_BARRIO": "BATALLON CALDAS"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "006208",
        "NOMBRE_BARRIO": "ORTEZAL"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "006211",
        "NOMBRE_BARRIO": "CUNDINAMARCA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "006212",
        "NOMBRE_BARRIO": "SALAZAR GOMEZ"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "006215",
        "NOMBRE_BARRIO": "PUENTE ARANDA"
    },
    {
        "CODIGO_LOCALIDAD": "16",
        "NOMBRE_LOCALIDAD": "PUENTE ARANDA",
        "CODIGO_BARRIO": "006218",
        "NOMBRE_BARRIO": "CENTRO INDUSTRIAL"
    },
    {
        "CODIGO_LOCALIDAD": "17",
        "NOMBRE_LOCALIDAD": "CANDELARIA",
        "CODIGO_BARRIO": "003103",
        "NOMBRE_BARRIO": "LAS AGUAS"
    },
    {
        "CODIGO_LOCALIDAD": "17",
        "NOMBRE_LOCALIDAD": "CANDELARIA",
        "CODIGO_BARRIO": "003104",
        "NOMBRE_BARRIO": "LA CONCORDIA"
    },
    {
        "CODIGO_LOCALIDAD": "17",
        "NOMBRE_LOCALIDAD": "CANDELARIA",
        "CODIGO_BARRIO": "003105",
        "NOMBRE_BARRIO": "EGIPTO"
    },
    {
        "CODIGO_LOCALIDAD": "17",
        "NOMBRE_LOCALIDAD": "CANDELARIA",
        "CODIGO_BARRIO": "003106",
        "NOMBRE_BARRIO": "CENTRO ADMINISTRATIVO"
    },
    {
        "CODIGO_LOCALIDAD": "17",
        "NOMBRE_LOCALIDAD": "CANDELARIA",
        "CODIGO_BARRIO": "003110",
        "NOMBRE_BARRIO": "LA CATEDRAL"
    },
    {
        "CODIGO_LOCALIDAD": "17",
        "NOMBRE_LOCALIDAD": "CANDELARIA",
        "CODIGO_BARRIO": "003203",
        "NOMBRE_BARRIO": "SANTA BARBARA"
    },
    {
        "CODIGO_LOCALIDAD": "17",
        "NOMBRE_LOCALIDAD": "CANDELARIA",
        "CODIGO_BARRIO": "003204",
        "NOMBRE_BARRIO": "BELEN"
    },
    {
        "CODIGO_LOCALIDAD": "17",
        "NOMBRE_LOCALIDAD": "CANDELARIA",
        "CODIGO_BARRIO": "008114",
        "NOMBRE_BARRIO": "PARQUE NACIONAL URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001401",
        "NOMBRE_BARRIO": "SAN JOSE SUR"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001402",
        "NOMBRE_BARRIO": "GUSTAVO RESTREPO"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001403",
        "NOMBRE_BARRIO": "HOSPITAL SAN CARLOS"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001404",
        "NOMBRE_BARRIO": "SOSIEGO SUR"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001410",
        "NOMBRE_BARRIO": "MARCO FIDEL SUAREZ"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001411",
        "NOMBRE_BARRIO": "SAN JORGE SUR"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001412",
        "NOMBRE_BARRIO": "GRANJAS SAN PABLO"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001413",
        "NOMBRE_BARRIO": "LA RESURRECCION"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001414",
        "NOMBRE_BARRIO": "MOLINOS DEL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001415",
        "NOMBRE_BARRIO": "MARCO FIDEL SUAREZ I"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001416",
        "NOMBRE_BARRIO": "SAN AGUSTIN"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001417",
        "NOMBRE_BARRIO": "LOS MOLINOS"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001418",
        "NOMBRE_BARRIO": "MARRUECOS"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001419",
        "NOMBRE_BARRIO": "CALLEJON SANTA BARBARA"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001420",
        "NOMBRE_BARRIO": "EL PLAYON"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001421",
        "NOMBRE_BARRIO": "DIANA TURBAY"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001422",
        "NOMBRE_BARRIO": "DIANA TURBAY ARRAYANES"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001423",
        "NOMBRE_BARRIO": "ARBOLEDA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001425",
        "NOMBRE_BARRIO": "GRANJAS DE SANTA SOFIA"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001426",
        "NOMBRE_BARRIO": "SAN LUIS"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001427",
        "NOMBRE_BARRIO": "PUERTO RICO"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001428",
        "NOMBRE_BARRIO": "CERROS DE ORIENTE"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001429",
        "NOMBRE_BARRIO": "GUIPARMA"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001431",
        "NOMBRE_BARRIO": "LA RESURRECCION I"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001432",
        "NOMBRE_BARRIO": "DIANA TURBAY CULTIVOS"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001434",
        "NOMBRE_BARRIO": "CARMEN DEL SOL"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "001435",
        "NOMBRE_BARRIO": "LOS ARRAYANES II"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002201",
        "NOMBRE_BARRIO": "OLAYA"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002202",
        "NOMBRE_BARRIO": "QUIROGA"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002203",
        "NOMBRE_BARRIO": "QUIROGA CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002204",
        "NOMBRE_BARRIO": "QUIROGA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002205",
        "NOMBRE_BARRIO": "SANTA LUCIA"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002209",
        "NOMBRE_BARRIO": "QUIROGA I"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002303",
        "NOMBRE_BARRIO": "CENTENARIO"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002304",
        "NOMBRE_BARRIO": "SANTIAGO PEREZ"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002305",
        "NOMBRE_BARRIO": "LIBERTADOR"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002306",
        "NOMBRE_BARRIO": "BRAVO PAEZ"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002307",
        "NOMBRE_BARRIO": "INGLES"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002308",
        "NOMBRE_BARRIO": "CLARET"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002310",
        "NOMBRE_BARRIO": "MURILLO TORO"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002311",
        "NOMBRE_BARRIO": "VILLA MAYOR"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002510",
        "NOMBRE_BARRIO": "LA PICOTA ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002511",
        "NOMBRE_BARRIO": "LA PICOTA"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002535",
        "NOMBRE_BARRIO": "PALERMO SUR"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002566",
        "NOMBRE_BARRIO": "LA PAZ"
    },
    {
        "CODIGO_LOCALIDAD": "18",
        "NOMBRE_LOCALIDAD": "RAFAEL URIBE URIBE",
        "CODIGO_BARRIO": "002637",
        "NOMBRE_BARRIO": "ARRAYANES VI"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002412",
        "NOMBRE_BARRIO": "VERONA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002414",
        "NOMBRE_BARRIO": "ISMAEL PERDOMO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002415",
        "NOMBRE_BARRIO": "MADELENA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002416",
        "NOMBRE_BARRIO": "EL ENSUENO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002417",
        "NOMBRE_BARRIO": "BARLOVENTO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002418",
        "NOMBRE_BARRIO": "LA ESTANCIA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002419",
        "NOMBRE_BARRIO": "RINCON DE LA VALVANERA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002420",
        "NOMBRE_BARRIO": "LA CORUNA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002421",
        "NOMBRE_BARRIO": "RAFAEL ESCAMILLA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002422",
        "NOMBRE_BARRIO": "ATLANTA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002423",
        "NOMBRE_BARRIO": "ESPINO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002424",
        "NOMBRE_BARRIO": "RINCON DE GALICIA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002426",
        "NOMBRE_BARRIO": "SANTO DOMINGO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002427",
        "NOMBRE_BARRIO": "GALICIA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002428",
        "NOMBRE_BARRIO": "PRIMAVERA II"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002429",
        "NOMBRE_BARRIO": "MARIA CANO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002430",
        "NOMBRE_BARRIO": "LAS BRISAS"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002431",
        "NOMBRE_BARRIO": "POTOSI"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002432",
        "NOMBRE_BARRIO": "ARBORIZADORA BAJA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002433",
        "NOMBRE_BARRIO": "EL PENON DEL CORTIJO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002434",
        "NOMBRE_BARRIO": "JERUSALEN"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002435",
        "NOMBRE_BARRIO": "EL CHIRCAL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002436",
        "NOMBRE_BARRIO": "BELLAVISTA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002437",
        "NOMBRE_BARRIO": "LA PRADERA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002438",
        "NOMBRE_BARRIO": "SIERRA MORENA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002439",
        "NOMBRE_BARRIO": "SIERRA MORENA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002440",
        "NOMBRE_BARRIO": "SIERRA MORENA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002441",
        "NOMBRE_BARRIO": "LOS TRES REYES I"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002442",
        "NOMBRE_BARRIO": "SANTA VIVIANA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002443",
        "NOMBRE_BARRIO": "LA PRIMAVERA I"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002444",
        "NOMBRE_BARRIO": "SIERRA MORENA II"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002445",
        "NOMBRE_BARRIO": "CARACOLI"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002446",
        "NOMBRE_BARRIO": "LOS TRES REYES"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002447",
        "NOMBRE_BARRIO": "EL MIRADOR DE LA ESTANCIA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002448",
        "NOMBRE_BARRIO": "PERDOMO ALTO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002449",
        "NOMBRE_BARRIO": "ESPINO I"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002450",
        "NOMBRE_BARRIO": "PERDOMO ALTO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002451",
        "NOMBRE_BARRIO": "SAN ANTONIO DEL MIRADOR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002453",
        "NOMBRE_BARRIO": "CIUDAD BOLIVAR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002455",
        "NOMBRE_BARRIO": "LA VALVANERA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002456",
        "NOMBRE_BARRIO": "QUIBA I"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002460",
        "NOMBRE_BARRIO": "ARBORIZADORA ALTA I"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002461",
        "NOMBRE_BARRIO": "ARBORIZADORA ALTA II"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002502",
        "NOMBRE_BARRIO": "MILLAN"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002504",
        "NOMBRE_BARRIO": "COMPARTIR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002513",
        "NOMBRE_BARRIO": "LAS ACACIAS"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002514",
        "NOMBRE_BARRIO": "MEISSEN"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002515",
        "NOMBRE_BARRIO": "SAN FRANCISCO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002516",
        "NOMBRE_BARRIO": "MEXICO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002517",
        "NOMBRE_BARRIO": "LUCERO DEL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002518",
        "NOMBRE_BARRIO": "RONDA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002520",
        "NOMBRE_BARRIO": "LAS MANAS"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002521",
        "NOMBRE_BARRIO": "LUCERO ALTO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002522",
        "NOMBRE_BARRIO": "EL MOCHUELO ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002523",
        "NOMBRE_BARRIO": "LA ALAMEDA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002530",
        "NOMBRE_BARRIO": "QUIBA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002531",
        "NOMBRE_BARRIO": "CANDELARIA LA NUEVA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002532",
        "NOMBRE_BARRIO": "QUINTAS DEL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002533",
        "NOMBRE_BARRIO": "SOTAVENTO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002534",
        "NOMBRE_BARRIO": "CASA DE TEJA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002539",
        "NOMBRE_BARRIO": "SAN RAFAEL"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002540",
        "NOMBRE_BARRIO": "ESTRELLA DEL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002544",
        "NOMBRE_BARRIO": "LOS LAURELES II"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002545",
        "NOMBRE_BARRIO": "EL SATELITE"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002546",
        "NOMBRE_BARRIO": "NACIONES UNIDAS"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002547",
        "NOMBRE_BARRIO": "EL TESORO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002549",
        "NOMBRE_BARRIO": "VILLA GLORIA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002552",
        "NOMBRE_BARRIO": "JUAN PABLO II"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002553",
        "NOMBRE_BARRIO": "SUMAPAZ"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002554",
        "NOMBRE_BARRIO": "GIBRALTAR SUR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002557",
        "NOMBRE_BARRIO": "JUAN JOSE RONDON"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002558",
        "NOMBRE_BARRIO": "VILLAS EL DIAMANTE"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002559",
        "NOMBRE_BARRIO": "LOS ALPES SUR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002560",
        "NOMBRE_BARRIO": "BELLAVISTA LUCERO ALTO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002561",
        "NOMBRE_BARRIO": "CORDILLERA DEL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002562",
        "NOMBRE_BARRIO": "CENTRAL DE MEZCLAS"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002564",
        "NOMBRE_BARRIO": "EL MINUTO DE MARIA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002565",
        "NOMBRE_BARRIO": "PARAISO QUIBA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002568",
        "NOMBRE_BARRIO": "ARBORIZADORA ALTA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002569",
        "NOMBRE_BARRIO": "ARBORIZADORA ALTA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002570",
        "NOMBRE_BARRIO": "EL MIRADOR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002573",
        "NOMBRE_BARRIO": "ARBORIZADORA ALTA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002574",
        "NOMBRE_BARRIO": "CEDRITOS DEL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002575",
        "NOMBRE_BARRIO": "CERRO COLORADO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002578",
        "NOMBRE_BARRIO": "ARABIA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002579",
        "NOMBRE_BARRIO": "VILLA CANDELARIA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002580",
        "NOMBRE_BARRIO": "BELLA FLOR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002581",
        "NOMBRE_BARRIO": "LA TORRE"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002582",
        "NOMBRE_BARRIO": "BELLA FLOR SUR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002583",
        "NOMBRE_BARRIO": "QUIBA URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002584",
        "NOMBRE_BARRIO": "EL MOCHUELO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002585",
        "NOMBRE_BARRIO": "EL MOCHUELO URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002587",
        "NOMBRE_BARRIO": "BRISAS DEL VOLADOR"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002588",
        "NOMBRE_BARRIO": "EL MOCHUELO III URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002591",
        "NOMBRE_BARRIO": "BRAZUELOS OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002595",
        "NOMBRE_BARRIO": "LAGUNITAS URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002596",
        "NOMBRE_BARRIO": "MOCHUELO ALTO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "002598",
        "NOMBRE_BARRIO": "EL MOCHUELO II URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "004563",
        "NOMBRE_BARRIO": "GUADALUPE"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104102",
        "NOMBRE_BARRIO": "PASQUILLITA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104104",
        "NOMBRE_BARRIO": "PASQUILLA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104107",
        "NOMBRE_BARRIO": "BELLA FLOR SUR RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104108",
        "NOMBRE_BARRIO": "LAS MERCEDES"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104110",
        "NOMBRE_BARRIO": "QUIBA BAJO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104112",
        "NOMBRE_BARRIO": "SANTA ROSA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104113",
        "NOMBRE_BARRIO": "SANTA BARBARA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104114",
        "NOMBRE_BARRIO": "QUIBA ALTO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104118",
        "NOMBRE_BARRIO": "CIUDAD BOLIVAR RURAL III"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104119",
        "NOMBRE_BARRIO": "CIUDAD BOLIVAR RURAL I"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104127",
        "NOMBRE_BARRIO": "EL MOCHUELO IV"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104128",
        "NOMBRE_BARRIO": "EL MOCHUELO III"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104129",
        "NOMBRE_BARRIO": "EL MOCHUELO II"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104130",
        "NOMBRE_BARRIO": "BRAZUELOS OCCIDENTAL RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "104131",
        "NOMBRE_BARRIO": "MOCHUELO ALTO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "204104",
        "NOMBRE_BARRIO": "PASQUILLA URBANA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "204106",
        "NOMBRE_BARRIO": "NUEVA ESPERANZA"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "204107",
        "NOMBRE_BARRIO": "EL MIRADOR II"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "204128",
        "NOMBRE_BARRIO": "QUIBA RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "204129",
        "NOMBRE_BARRIO": "EL MOCHUELO II NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "204131",
        "NOMBRE_BARRIO": "MOCHUELO ALTO URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "19",
        "NOMBRE_LOCALIDAD": "CIUDAD BOLIVAR",
        "CODIGO_BARRIO": "204304",
        "NOMBRE_BARRIO": "LAGUNITAS"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008110",
        "NOMBRE_BARRIO": "EL PARAISO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008111",
        "NOMBRE_BARRIO": "CATALUNA"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008112",
        "NOMBRE_BARRIO": "SUCRE"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008201",
        "NOMBRE_BARRIO": "QUINTA CAMACHO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008202",
        "NOMBRE_BARRIO": "EMAUS"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008203",
        "NOMBRE_BARRIO": "LAS ACACIAS"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008204",
        "NOMBRE_BARRIO": "GRANADA"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008205",
        "NOMBRE_BARRIO": "MARIA CRISTINA"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008206",
        "NOMBRE_BARRIO": "LA SALLE"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008207",
        "NOMBRE_BARRIO": "BOSQUE CALDERON"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008208",
        "NOMBRE_BARRIO": "PARDO RUBIO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008211",
        "NOMBRE_BARRIO": "JUAN XXIII"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008212",
        "NOMBRE_BARRIO": "MARLY"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008213",
        "NOMBRE_BARRIO": "CHAPINERO CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008214",
        "NOMBRE_BARRIO": "CHAPINERO NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008215",
        "NOMBRE_BARRIO": "INGEMAR"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008218",
        "NOMBRE_BARRIO": "INGEMAR ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008220",
        "NOMBRE_BARRIO": "SIBERIA CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008221",
        "NOMBRE_BARRIO": "SIBERIA"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008301",
        "NOMBRE_BARRIO": "CHICO NORTE II SECTOR"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008302",
        "NOMBRE_BARRIO": "SEMINARIO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008303",
        "NOMBRE_BARRIO": "EL REFUGIO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008304",
        "NOMBRE_BARRIO": "LOS ROSALES"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008305",
        "NOMBRE_BARRIO": "BELLAVISTA"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008306",
        "NOMBRE_BARRIO": "PORCIUNCULA"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008307",
        "NOMBRE_BARRIO": "CHICO NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008308",
        "NOMBRE_BARRIO": "EL CHICO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008309",
        "NOMBRE_BARRIO": "LA CABRERA"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008310",
        "NOMBRE_BARRIO": "EL RETIRO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008311",
        "NOMBRE_BARRIO": "EL NOGAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008312",
        "NOMBRE_BARRIO": "ESPARTILLAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008313",
        "NOMBRE_BARRIO": "LAGO GAITAN"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008314",
        "NOMBRE_BARRIO": "ANTIGUO COUNTRY"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008315",
        "NOMBRE_BARRIO": "CHICO NORTE III SECTOR"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008316",
        "NOMBRE_BARRIO": "SAN LUIS ALTOS DEL CABO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008317",
        "NOMBRE_BARRIO": "SAN ISIDRO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008319",
        "NOMBRE_BARRIO": "EL BAGAZAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "008422",
        "NOMBRE_BARRIO": "PARAMO URBANO III"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "101101",
        "NOMBRE_BARRIO": "EL BAGAZAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "101103",
        "NOMBRE_BARRIO": "INGEMAR ORIENTAL RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "101110",
        "NOMBRE_BARRIO": "PARAMO I RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "101502",
        "NOMBRE_BARRIO": "SIBERIA"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "108109",
        "NOMBRE_BARRIO": "PARAMO I"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "201108",
        "NOMBRE_BARRIO": "EL REFUGIO II"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "201502",
        "NOMBRE_BARRIO": "SIBERIA RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "201503",
        "NOMBRE_BARRIO": "SIBERIA  II"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "208101",
        "NOMBRE_BARRIO": "PARAMO IV"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "208104",
        "NOMBRE_BARRIO": "PARAMO URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "208107",
        "NOMBRE_BARRIO": "LA ESPERANZA"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "208108",
        "NOMBRE_BARRIO": "SAN ISIDRO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "208110",
        "NOMBRE_BARRIO": "SAN LUIS ALTOS DEL CABO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "2",
        "NOMBRE_LOCALIDAD": "CHAPINERO",
        "CODIGO_BARRIO": "208111",
        "NOMBRE_BARRIO": "SAN ISIDRO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103101",
        "NOMBRE_BARRIO": "SANTA ROSA ALTA"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103102",
        "NOMBRE_BARRIO": "TAQUECITOS"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103103",
        "NOMBRE_BARRIO": "SANTA ROSA BAJA"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103104",
        "NOMBRE_BARRIO": "LAS ANIMAS"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103105",
        "NOMBRE_BARRIO": "LAS AURAS"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103106",
        "NOMBRE_BARRIO": "NAZARETH"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103107",
        "NOMBRE_BARRIO": "LAS PALMAS"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103108",
        "NOMBRE_BARRIO": "LOS RIOS"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103109",
        "NOMBRE_BARRIO": "LAS SOPAS"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103201",
        "NOMBRE_BARRIO": "BETANIA"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103202",
        "NOMBRE_BARRIO": "EL ISTMO"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103203",
        "NOMBRE_BARRIO": "EL TABACO"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103204",
        "NOMBRE_BARRIO": "LAGUNA VERDE"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103205",
        "NOMBRE_BARRIO": "RAIZAL"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "103206",
        "NOMBRE_BARRIO": "PENALISA"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109101",
        "NOMBRE_BARRIO": "EL TOLDO"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109102",
        "NOMBRE_BARRIO": "SAN ANTONIO"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109103",
        "NOMBRE_BARRIO": "LAS VEGAS"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109104",
        "NOMBRE_BARRIO": "SAN JUAN"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109105",
        "NOMBRE_BARRIO": "SANTO DOMINGO"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109106",
        "NOMBRE_BARRIO": "LA UNION"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109107",
        "NOMBRE_BARRIO": "CHORRERAS"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109109",
        "NOMBRE_BARRIO": "TUNAL ALTO"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109110",
        "NOMBRE_BARRIO": "CONCEPCION"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109111",
        "NOMBRE_BARRIO": "SAN JOSE"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109112",
        "NOMBRE_BARRIO": "CAPITOLIO"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109113",
        "NOMBRE_BARRIO": "LAGUNITAS"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109114",
        "NOMBRE_BARRIO": "TUNAL BAJO"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "109116",
        "NOMBRE_BARRIO": "NUEVA GRANADA"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "203106",
        "NOMBRE_BARRIO": "NAZARETH URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "209104",
        "NOMBRE_BARRIO": "SAN JUAN URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "20",
        "NOMBRE_LOCALIDAD": "SUMAPAZ",
        "CODIGO_BARRIO": "209106",
        "NOMBRE_BARRIO": "LA UNION URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003101",
        "NOMBRE_BARRIO": "LA ALAMEDA"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003102",
        "NOMBRE_BARRIO": "LAS NIEVES"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003107",
        "NOMBRE_BARRIO": "SANTA INES"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003108",
        "NOMBRE_BARRIO": "LA CAPUCHINA"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003109",
        "NOMBRE_BARRIO": "VERACRUZ"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003201",
        "NOMBRE_BARRIO": "SAN BERNARDINO"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003202",
        "NOMBRE_BARRIO": "LAS CRUCES"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003205",
        "NOMBRE_BARRIO": "EL GUAVIO"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003206",
        "NOMBRE_BARRIO": "LA PENA"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003207",
        "NOMBRE_BARRIO": "LOS LACHES"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003208",
        "NOMBRE_BARRIO": "EL ROCIO"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003209",
        "NOMBRE_BARRIO": "EL DORADO"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003210",
        "NOMBRE_BARRIO": "RAMIREZ"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003211",
        "NOMBRE_BARRIO": "GIRARDOT"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003212",
        "NOMBRE_BARRIO": "LOURDES"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "003215",
        "NOMBRE_BARRIO": "SAN FRANCISCO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "008101",
        "NOMBRE_BARRIO": "SAGRADO CORAZON"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "008102",
        "NOMBRE_BARRIO": "PARQUE NACIONAL"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "008103",
        "NOMBRE_BARRIO": "LA MERCED"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "008104",
        "NOMBRE_BARRIO": "LA PERSEVERANCIA"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "008105",
        "NOMBRE_BARRIO": "LA MACARENA"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "008106",
        "NOMBRE_BARRIO": "BOSQUE IZQUIERDO"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "008107",
        "NOMBRE_BARRIO": "SAN DIEGO"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "008108",
        "NOMBRE_BARRIO": "SAMPER"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "008109",
        "NOMBRE_BARRIO": "SAN MARTIN"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "101201",
        "NOMBRE_BARRIO": "HOYA TEUSACA"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "101405",
        "NOMBRE_BARRIO": "PARQUE NACIONAL ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "3",
        "NOMBRE_LOCALIDAD": "SANTA FE",
        "CODIGO_BARRIO": "201404",
        "NOMBRE_BARRIO": "LA PEÑA RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001101",
        "NOMBRE_BARRIO": "LAS BRISAS"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001102",
        "NOMBRE_BARRIO": "BUENOS AIRES"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001103",
        "NOMBRE_BARRIO": "VITELMA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001104",
        "NOMBRE_BARRIO": "MOLINOS DE ORIENTE"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001106",
        "NOMBRE_BARRIO": "SAN BLAS"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001107",
        "NOMBRE_BARRIO": "LAS MERCEDES"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001108",
        "NOMBRE_BARRIO": "SAN CRISTOBAL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001109",
        "NOMBRE_BARRIO": "LA MARIA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001110",
        "NOMBRE_BARRIO": "SAN JAVIER"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001111",
        "NOMBRE_BARRIO": "SANTA ANA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001112",
        "NOMBRE_BARRIO": "PRIMERO DE MAYO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001113",
        "NOMBRE_BARRIO": "SAN BLAS II"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001114",
        "NOMBRE_BARRIO": "VELODROMO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001115",
        "NOMBRE_BARRIO": "MONTE CARLO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001116",
        "NOMBRE_BARRIO": "TIBAQUE URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001205",
        "NOMBRE_BARRIO": "SOCIEGO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001206",
        "NOMBRE_BARRIO": "QUINTA RAMOS"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001207",
        "NOMBRE_BARRIO": "NARINO SUR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001209",
        "NOMBRE_BARRIO": "MODELO SUR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001210",
        "NOMBRE_BARRIO": "CALVO SUR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001301",
        "NOMBRE_BARRIO": "GRANADA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001302",
        "NOMBRE_BARRIO": "MONTEBELLO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001303",
        "NOMBRE_BARRIO": "CORDOBA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001304",
        "NOMBRE_BARRIO": "BELLO HORIZONTE"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001305",
        "NOMBRE_BARRIO": "ATENAS"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001306",
        "NOMBRE_BARRIO": "SAN PEDRO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001307",
        "NOMBRE_BARRIO": "RAMAJAL"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001308",
        "NOMBRE_BARRIO": "SANTA INES SUR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001309",
        "NOMBRE_BARRIO": "SAN VICENTE"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001310",
        "NOMBRE_BARRIO": "LA VICTORIA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001311",
        "NOMBRE_BARRIO": "LA GLORIA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001312",
        "NOMBRE_BARRIO": "LOS ALPES"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001313",
        "NOMBRE_BARRIO": "BELLAVISTA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001314",
        "NOMBRE_BARRIO": "SAN JOSE SUR ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001315",
        "NOMBRE_BARRIO": "ALTAMIRA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001316",
        "NOMBRE_BARRIO": "MORALBA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001317",
        "NOMBRE_BARRIO": "PUENTE COLORADO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001318",
        "NOMBRE_BARRIO": "QUINDIO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001319",
        "NOMBRE_BARRIO": "LA GLORIA ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001320",
        "NOMBRE_BARRIO": "NUEVA GLORIA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001321",
        "NOMBRE_BARRIO": "SAN MARTIN SUR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001322",
        "NOMBRE_BARRIO": "SAN RAFAEL USME"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001323",
        "NOMBRE_BARRIO": "NUEVA DELHI"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001324",
        "NOMBRE_BARRIO": "EL PINAR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001325",
        "NOMBRE_BARRIO": "JUAN REY (LA PAZ)"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001327",
        "NOMBRE_BARRIO": "LOS LIBERTADORES"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001328",
        "NOMBRE_BARRIO": "SANTA RITA SUR ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001329",
        "NOMBRE_BARRIO": "EL PARAISO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001330",
        "NOMBRE_BARRIO": "CANADA O GUIRA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001331",
        "NOMBRE_BARRIO": "ALTOS DEL ZUQUE"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001332",
        "NOMBRE_BARRIO": "VILLABEL"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001333",
        "NOMBRE_BARRIO": "ALTOS DEL POBLADO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001334",
        "NOMBRE_BARRIO": "LAS GAVIOTAS"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001338",
        "NOMBRE_BARRIO": "LA BELLEZA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001339",
        "NOMBRE_BARRIO": "BOSQUE DE LOS ALPES"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001342",
        "NOMBRE_BARRIO": "ALTOS DEL ZIPA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001346",
        "NOMBRE_BARRIO": "CIUDAD LONDRES I"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001350",
        "NOMBRE_BARRIO": "YOMASA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001352",
        "NOMBRE_BARRIO": "GUACAMAYAS II"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001353",
        "NOMBRE_BARRIO": "GUACAMAYAS IV"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001354",
        "NOMBRE_BARRIO": "GUACAMAYAS III"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001355",
        "NOMBRE_BARRIO": "LAS GUACAMAYAS I"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001356",
        "NOMBRE_BARRIO": "VILLA DEL CERRO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001357",
        "NOMBRE_BARRIO": "SANTA INES SUR II"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001358",
        "NOMBRE_BARRIO": "CHIGUAZA URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001362",
        "NOMBRE_BARRIO": "ARBOLEDA SANTA TERESITA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001405",
        "NOMBRE_BARRIO": "VEINTE DE JULIO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001406",
        "NOMBRE_BARRIO": "SAN ISIDRO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001407",
        "NOMBRE_BARRIO": "SURAMERICA"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001408",
        "NOMBRE_BARRIO": "VILLA DE LOS ALPES"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001409",
        "NOMBRE_BARRIO": "LAS LOMAS"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001424",
        "NOMBRE_BARRIO": "BARCELONA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "001430",
        "NOMBRE_BARRIO": "VILLA DE LOS ALPES I"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "101301",
        "NOMBRE_BARRIO": "HOYA SAN CRISTOBAL"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "101302",
        "NOMBRE_BARRIO": "MOLINO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "101306",
        "NOMBRE_BARRIO": "TIBAQUE"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "101309",
        "NOMBRE_BARRIO": "LA ARBOLEDA RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "201302",
        "NOMBRE_BARRIO": "EL TRIANGULO"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "201304",
        "NOMBRE_BARRIO": "AGUAS CLARAS"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "201305",
        "NOMBRE_BARRIO": "LOS LAURELES I"
    },
    {
        "CODIGO_LOCALIDAD": "4",
        "NOMBRE_LOCALIDAD": "SAN CRISTOBAL",
        "CODIGO_BARRIO": "201306",
        "NOMBRE_BARRIO": "TIBAQUE I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "001335",
        "NOMBRE_BARRIO": "VILLA DIANA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "001336",
        "NOMBRE_BARRIO": "JUAN JOSE RONDON I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "001337",
        "NOMBRE_BARRIO": "SAN PEDRO SUR"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "001340",
        "NOMBRE_BARRIO": "TIHUAQUE"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "001344",
        "NOMBRE_BARRIO": "LOS SOCHES"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "001345",
        "NOMBRE_BARRIO": "DONA LILIANA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "001347",
        "NOMBRE_BARRIO": "JUAN REY SUR"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "001348",
        "NOMBRE_BARRIO": "LA CABANA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "001349",
        "NOMBRE_BARRIO": "CIUDAD LONDRES"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002503",
        "NOMBRE_BARRIO": "LA AURORA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002505",
        "NOMBRE_BARRIO": "NUEVO SAN ANDRES"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002506",
        "NOMBRE_BARRIO": "GRAN YOMASA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002519",
        "NOMBRE_BARRIO": "DANUBIO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002524",
        "NOMBRE_BARRIO": "LA ANDREA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002525",
        "NOMBRE_BARRIO": "BARRANQUILLITA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002526",
        "NOMBRE_BARRIO": "SANTA LIBRADA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002527",
        "NOMBRE_BARRIO": "LA CABANA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002528",
        "NOMBRE_BARRIO": "SANTA LIBRADA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002529",
        "NOMBRE_BARRIO": "MARICHUELA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002536",
        "NOMBRE_BARRIO": "MONTEBLANCO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002537",
        "NOMBRE_BARRIO": "CHUNIZA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002538",
        "NOMBRE_BARRIO": "USMINIA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002541",
        "NOMBRE_BARRIO": "GRANADA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002542",
        "NOMBRE_BARRIO": "SERRANIAS"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002543",
        "NOMBRE_BARRIO": "COMUNEROS"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002548",
        "NOMBRE_BARRIO": "EL VIRREY"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002550",
        "NOMBRE_BARRIO": "DUITAMA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002551",
        "NOMBRE_BARRIO": "ALASKA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002555",
        "NOMBRE_BARRIO": "SAN JUAN BAUTISTA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002556",
        "NOMBRE_BARRIO": "DESARROLLO BRAZUELOS I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002563",
        "NOMBRE_BARRIO": "EL PEDREGAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002571",
        "NOMBRE_BARRIO": "ANTONIO JOSE DE SUCRE"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002572",
        "NOMBRE_BARRIO": "SALAZAR USME"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002576",
        "NOMBRE_BARRIO": "SERRANIAS I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002577",
        "NOMBRE_BARRIO": "DANUBIO II"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002586",
        "NOMBRE_BARRIO": "YOMASA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002589",
        "NOMBRE_BARRIO": "PORVENIR"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002590",
        "NOMBRE_BARRIO": "LA FISCALA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002592",
        "NOMBRE_BARRIO": "DESARROLLO BRAZUELOS"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002593",
        "NOMBRE_BARRIO": "TUNJUELITO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002594",
        "NOMBRE_BARRIO": "VILLA ISRAEL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002597",
        "NOMBRE_BARRIO": "LA FISCALA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002599",
        "NOMBRE_BARRIO": "CENTRO USME URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002601",
        "NOMBRE_BARRIO": "ARRAYANES I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002602",
        "NOMBRE_BARRIO": "FISCALA ALTA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002604",
        "NOMBRE_BARRIO": "LOS OLIVARES"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002605",
        "NOMBRE_BARRIO": "BOLONIA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002606",
        "NOMBRE_BARRIO": "EL CURUBO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002607",
        "NOMBRE_BARRIO": "LA ESPERANZA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002608",
        "NOMBRE_BARRIO": "EL BOSQUE CENTRAL I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002609",
        "NOMBRE_BARRIO": "EL BOSQUE"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002610",
        "NOMBRE_BARRIO": "CHAPINERITO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002611",
        "NOMBRE_BARRIO": "CHARALA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002612",
        "NOMBRE_BARRIO": "LA COMUNA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002613",
        "NOMBRE_BARRIO": "EL PROGRESO USME"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002614",
        "NOMBRE_BARRIO": "LA ORQUIDEA DE USME"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002615",
        "NOMBRE_BARRIO": "LA ESPERANZA DE USME"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002616",
        "NOMBRE_BARRIO": "EL NUEVO PORTAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002617",
        "NOMBRE_BARRIO": "EL REFUGIO I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002618",
        "NOMBRE_BARRIO": "PUERTA AL LLANO DE USME"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002619",
        "NOMBRE_BARRIO": "VILLA ANITA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002620",
        "NOMBRE_BARRIO": "EL NUEVO PORTAL II"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002621",
        "NOMBRE_BARRIO": "SAN FELIPE DE USME"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002622",
        "NOMBRE_BARRIO": "TOCAIMITA ORIENTAL I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002625",
        "NOMBRE_BARRIO": "ARRAYANES V"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002626",
        "NOMBRE_BARRIO": "BOLONIA I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002627",
        "NOMBRE_BARRIO": "LA REFORMA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002628",
        "NOMBRE_BARRIO": "EL BOSQUE SUR ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002629",
        "NOMBRE_BARRIO": "EL NEVADO II"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002630",
        "NOMBRE_BARRIO": "EL PORTAL DEL DIVINO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002631",
        "NOMBRE_BARRIO": "EL NEVADO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002632",
        "NOMBRE_BARRIO": "LA REQUILINA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002633",
        "NOMBRE_BARRIO": "LAS VIOLETAS"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002634",
        "NOMBRE_BARRIO": "EL TUNO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002635",
        "NOMBRE_BARRIO": "BRISAS DEL LLANO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002636",
        "NOMBRE_BARRIO": "LA HUERTA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002640",
        "NOMBRE_BARRIO": "EL PEDREGAL II"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "002642",
        "NOMBRE_BARRIO": "EL PORTAL URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "101307",
        "NOMBRE_BARRIO": "TIHUAQUE RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "101308",
        "NOMBRE_BARRIO": "CHIGUAZA RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102110",
        "NOMBRE_BARRIO": "EL PORVENIR DE LOS SOCHES"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102111",
        "NOMBRE_BARRIO": "LOS ARRAYANES"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102112",
        "NOMBRE_BARRIO": "EL HATO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102113",
        "NOMBRE_BARRIO": "LAS MARGARITAS"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102114",
        "NOMBRE_BARRIO": "LOS ANDES"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102115",
        "NOMBRE_BARRIO": "LA UNION"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102116",
        "NOMBRE_BARRIO": "CHISACA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102118",
        "NOMBRE_BARRIO": "LAS VIOLETAS RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102122",
        "NOMBRE_BARRIO": "LA REQUILINA RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102211",
        "NOMBRE_BARRIO": "LILIANA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102309",
        "NOMBRE_BARRIO": "EL BOSQUE SUR ORIENTAL RURAL II"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102310",
        "NOMBRE_BARRIO": "EL BOSQUE SUR ORIENTAL RURAL I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102312",
        "NOMBRE_BARRIO": "CIUDAD LONDRES RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102401",
        "NOMBRE_BARRIO": "LOS ARRAYANES"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102405",
        "NOMBRE_BARRIO": "PEPINITOS"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102406",
        "NOMBRE_BARRIO": "EL BOSQUE CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102407",
        "NOMBRE_BARRIO": "TIBAQUE SUR"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102412",
        "NOMBRE_BARRIO": "TOCAIMITA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102502",
        "NOMBRE_BARRIO": "EL UVAL RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102506",
        "NOMBRE_BARRIO": "PORTAL RURAL II"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102601",
        "NOMBRE_BARRIO": "CENTRO USME RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102602",
        "NOMBRE_BARRIO": "CENTRO USME RURAL I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102603",
        "NOMBRE_BARRIO": "CENTRO USME RURAL II"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102604",
        "NOMBRE_BARRIO": "LA REQUILINA RURAL II"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102707",
        "NOMBRE_BARRIO": "OLARTE"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102808",
        "NOMBRE_BARRIO": "LA REGADERA"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102909",
        "NOMBRE_BARRIO": "SAN BENITO"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102910",
        "NOMBRE_BARRIO": "ARRAYAN"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "102911",
        "NOMBRE_BARRIO": "CURUBITAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "201309",
        "NOMBRE_BARRIO": "TIBAQUE II"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "202203",
        "NOMBRE_BARRIO": "BOLONIA I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "202204",
        "NOMBRE_BARRIO": "LA ESPERANZA SUR I"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "202404",
        "NOMBRE_BARRIO": "SAN FELIPE DE USME RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "202410",
        "NOMBRE_BARRIO": "TOCAIMITA ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "202502",
        "NOMBRE_BARRIO": "EL UVAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "202506",
        "NOMBRE_BARRIO": "EL NUEVO PORTAL II RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "202511",
        "NOMBRE_BARRIO": "PUERTA AL LLANO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "5",
        "NOMBRE_LOCALIDAD": "USME",
        "CODIGO_BARRIO": "202601",
        "NOMBRE_BARRIO": "CENTRO USME"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002401",
        "NOMBRE_BARRIO": "TUNAL ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002402",
        "NOMBRE_BARRIO": "SAN VICENTE FERRER"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002403",
        "NOMBRE_BARRIO": "VENECIA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002404",
        "NOMBRE_BARRIO": "VENECIA"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002405",
        "NOMBRE_BARRIO": "ESCUELA GENERAL SANTANDER"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002406",
        "NOMBRE_BARRIO": "SAMORE"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002407",
        "NOMBRE_BARRIO": "EL CARMEN"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002408",
        "NOMBRE_BARRIO": "FATIMA"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002409",
        "NOMBRE_BARRIO": "NUEVO MUZU"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002410",
        "NOMBRE_BARRIO": "PARQUE EL TUNAL"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002411",
        "NOMBRE_BARRIO": "ISLA DEL SOL"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002413",
        "NOMBRE_BARRIO": "MUZU"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002501",
        "NOMBRE_BARRIO": "SAN CARLOS"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002507",
        "NOMBRE_BARRIO": "ABRAHAM LINCOLN"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002508",
        "NOMBRE_BARRIO": "TUNJUELITO"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002509",
        "NOMBRE_BARRIO": "SAN BENITO"
    },
    {
        "CODIGO_LOCALIDAD": "6",
        "NOMBRE_LOCALIDAD": "TUNJUELITO",
        "CODIGO_BARRIO": "002512",
        "NOMBRE_BARRIO": "AREA ARTILLERIA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004513",
        "NOMBRE_BARRIO": "SAN DIEGO-BOSA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004519",
        "NOMBRE_BARRIO": "ESCOCIA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004520",
        "NOMBRE_BARRIO": "LA PAZ BOSA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004521",
        "NOMBRE_BARRIO": "LA ESTACION BOSA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004522",
        "NOMBRE_BARRIO": "BOSA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004523",
        "NOMBRE_BARRIO": "JIMENEZ DE QUESADA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004524",
        "NOMBRE_BARRIO": "SAN PABLO BOSA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004526",
        "NOMBRE_BARRIO": "BOSA NOVA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004527",
        "NOMBRE_BARRIO": "NUEVA GRANADA BOSA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004528",
        "NOMBRE_BARRIO": "PASO ANCHO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004529",
        "NOMBRE_BARRIO": "CEMENTERIO JARDINES APOGEO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004533",
        "NOMBRE_BARRIO": "EL RETAZO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004538",
        "NOMBRE_BARRIO": "OLARTE"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004539",
        "NOMBRE_BARRIO": "GRAN COLOMBIANO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004546",
        "NOMBRE_BARRIO": "VILLA DEL RIO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004550",
        "NOMBRE_BARRIO": "JOSE MARIA CARBONEL"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004552",
        "NOMBRE_BARRIO": "GUALOCHE"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004553",
        "NOMBRE_BARRIO": "ANDALUCIA II"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004561",
        "NOMBRE_BARRIO": "EL JARDIN"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004567",
        "NOMBRE_BARRIO": "VILLAS DEL PROGRESO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004569",
        "NOMBRE_BARRIO": "JOSE ANTONIO GALAN"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004570",
        "NOMBRE_BARRIO": "ANTONIA SANTOS"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004573",
        "NOMBRE_BARRIO": "CIUDADELA EL RECREO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004574",
        "NOMBRE_BARRIO": "CHARLES DE GAULLE"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004575",
        "NOMBRE_BARRIO": "LOS LAURELES"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004577",
        "NOMBRE_BARRIO": "SAN BERNARDINO XXII URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004579",
        "NOMBRE_BARRIO": "ESCOCIA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004580",
        "NOMBRE_BARRIO": "BOSA NOVA EL PORVENIR"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004583",
        "NOMBRE_BARRIO": "ARGELIA II"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004586",
        "NOMBRE_BARRIO": "JIMENEZ DE QUESADA II SECTOR"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004587",
        "NOMBRE_BARRIO": "LOS SAUCES"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004588",
        "NOMBRE_BARRIO": "EL DANUBIO AZUL"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004589",
        "NOMBRE_BARRIO": "SAN PEDRO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004590",
        "NOMBRE_BARRIO": "SAN BERNARDINO XXV URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004591",
        "NOMBRE_BARRIO": "EL PORTAL DEL BRASIL"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004592",
        "NOMBRE_BARRIO": "SAN MARTIN"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004593",
        "NOMBRE_BARRIO": "LA LIBERTAD"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004594",
        "NOMBRE_BARRIO": "SAN ANTONIO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004595",
        "NOMBRE_BARRIO": "CHICO SUR"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004597",
        "NOMBRE_BARRIO": "SAN BERNARDINO I"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004598",
        "NOMBRE_BARRIO": "VILLA ANNY I"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004599",
        "NOMBRE_BARRIO": "VILLA ANNY II"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004602",
        "NOMBRE_BARRIO": "REMANSO URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004604",
        "NOMBRE_BARRIO": "BRASILIA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004605",
        "NOMBRE_BARRIO": "CHICALA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004616",
        "NOMBRE_BARRIO": "OSORIO X URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004622",
        "NOMBRE_BARRIO": "BRASIL"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004623",
        "NOMBRE_BARRIO": "SAN BERNARDINO POTRERITOS"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004624",
        "NOMBRE_BARRIO": "LA VEGA SAN BERNARDINO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004625",
        "NOMBRE_BARRIO": "EL REMANSO I"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004628",
        "NOMBRE_BARRIO": "BETANIA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004630",
        "NOMBRE_BARRIO": "PARCELA EL PORVENIR"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004631",
        "NOMBRE_BARRIO": "EL CORZO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004632",
        "NOMBRE_BARRIO": "LA CABANA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004633",
        "NOMBRE_BARRIO": "SANTA FE BOSA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004634",
        "NOMBRE_BARRIO": "CANAVERALEJO"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004635",
        "NOMBRE_BARRIO": "SAN BERNARDINO II"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004637",
        "NOMBRE_BARRIO": "LAS MARGARITAS"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004638",
        "NOMBRE_BARRIO": "CIUDADELA EL RECREO II"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004641",
        "NOMBRE_BARRIO": "LA INDEPENDENCIA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004642",
        "NOMBRE_BARRIO": "ISLANDIA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "004643",
        "NOMBRE_BARRIO": "EL CORZO I"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "105210",
        "NOMBRE_BARRIO": "OSORIO X"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "105223",
        "NOMBRE_BARRIO": "OSORIO XXIII"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "105314",
        "NOMBRE_BARRIO": "EL CORZO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "105317",
        "NOMBRE_BARRIO": "SAN BERNARDINO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "105318",
        "NOMBRE_BARRIO": "SAN BERNARDINO XVIII"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "105322",
        "NOMBRE_BARRIO": "SAN BERNARDINO XXII"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "105325",
        "NOMBRE_BARRIO": "SAN BERNARDINO XXV"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "205308",
        "NOMBRE_BARRIO": "CANAVERALEJO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "205316",
        "NOMBRE_BARRIO": "VILLA EMMA"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "205318",
        "NOMBRE_BARRIO": "SAN BERNARDINO XVIII"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "205319",
        "NOMBRE_BARRIO": "SAN BERNARDINO XIX"
    },
    {
        "CODIGO_LOCALIDAD": "7",
        "NOMBRE_LOCALIDAD": "BOSA",
        "CODIGO_BARRIO": "205320",
        "NOMBRE_BARRIO": "EL REMANSO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004316",
        "NOMBRE_BARRIO": "LAS MARGARITAS"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004501",
        "NOMBRE_BARRIO": "HIPOTECHO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004502",
        "NOMBRE_BARRIO": "HIPOTECHO OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004503",
        "NOMBRE_BARRIO": "PROVIVIENDA ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004504",
        "NOMBRE_BARRIO": "PROVIVIENDA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004505",
        "NOMBRE_BARRIO": "PROVIVIENDA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004506",
        "NOMBRE_BARRIO": "LA CAMPINA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004507",
        "NOMBRE_BARRIO": "CIUDAD KENNEDY SUR"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004508",
        "NOMBRE_BARRIO": "CIUDAD KENNEDY OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004509",
        "NOMBRE_BARRIO": "CIUDAD KENNEDY"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004510",
        "NOMBRE_BARRIO": "CIUDAD KENNEDY ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004511",
        "NOMBRE_BARRIO": "CIUDAD KENNEDY CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004512",
        "NOMBRE_BARRIO": "TIMIZA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004514",
        "NOMBRE_BARRIO": "CIUDAD KENNEDY NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004515",
        "NOMBRE_BARRIO": "TUNDAMA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004516",
        "NOMBRE_BARRIO": "BOITA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004517",
        "NOMBRE_BARRIO": "JACQUELINE"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004518",
        "NOMBRE_BARRIO": "TIMIZA A"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004525",
        "NOMBRE_BARRIO": "PASTRANA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004530",
        "NOMBRE_BARRIO": "TIMIZA B"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004531",
        "NOMBRE_BARRIO": "MANDALAY"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004532",
        "NOMBRE_BARRIO": "LA CECILIA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004534",
        "NOMBRE_BARRIO": "ROMA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004535",
        "NOMBRE_BARRIO": "CLASS"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004536",
        "NOMBRE_BARRIO": "EL RUBI"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004537",
        "NOMBRE_BARRIO": "CASABLANCA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004540",
        "NOMBRE_BARRIO": "CATALINA II"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004541",
        "NOMBRE_BARRIO": "EL PARAISO BOSA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004542",
        "NOMBRE_BARRIO": "ALQUERIA LA FRAGUA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004543",
        "NOMBRE_BARRIO": "ALQUERIA LA FRAGUA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004544",
        "NOMBRE_BARRIO": "LAS DELICIAS"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004545",
        "NOMBRE_BARRIO": "CATALINA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004547",
        "NOMBRE_BARRIO": "CORABASTOS"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004548",
        "NOMBRE_BARRIO": "TECHO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004549",
        "NOMBRE_BARRIO": "NUEVA YORK"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004551",
        "NOMBRE_BARRIO": "HIPOTECHO SUR"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004554",
        "NOMBRE_BARRIO": "PATIO BONITO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004555",
        "NOMBRE_BARRIO": "PATIO BONITO II"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004556",
        "NOMBRE_BARRIO": "GRAN BRITALIA I"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004557",
        "NOMBRE_BARRIO": "GRAN BRITALIA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004558",
        "NOMBRE_BARRIO": "CAMPO HERMOSO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004559",
        "NOMBRE_BARRIO": "EL CARMELO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004560",
        "NOMBRE_BARRIO": "SAUCEDAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004562",
        "NOMBRE_BARRIO": "LLANO GRANDE"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004564",
        "NOMBRE_BARRIO": "TAIRONA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004565",
        "NOMBRE_BARRIO": "TOCAREMA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004566",
        "NOMBRE_BARRIO": "PATIO BONITO III"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004568",
        "NOMBRE_BARRIO": "JORGE URIBE BOTERO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004572",
        "NOMBRE_BARRIO": "CORREDOR FERREO DEL SUR"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004576",
        "NOMBRE_BARRIO": "CASA BLANCA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004578",
        "NOMBRE_BARRIO": "VILLA NELLY III SECTOR"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004581",
        "NOMBRE_BARRIO": "TIMIZA C"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004582",
        "NOMBRE_BARRIO": "RENANIA URAPANES"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004584",
        "NOMBRE_BARRIO": "SANTA CATALINA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004596",
        "NOMBRE_BARRIO": "ALQUERIA LA FRAGUA II"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004601",
        "NOMBRE_BARRIO": "CALANDAIMA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004603",
        "NOMBRE_BARRIO": "EL PARAISO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004607",
        "NOMBRE_BARRIO": "CIUDAD DE CALI"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004609",
        "NOMBRE_BARRIO": "LOS ALMENDROS"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004611",
        "NOMBRE_BARRIO": "EL JAZMIN"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004612",
        "NOMBRE_BARRIO": "DINDALITO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004613",
        "NOMBRE_BARRIO": "PROVIVIENDA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004614",
        "NOMBRE_BARRIO": "DINDALITO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004615",
        "NOMBRE_BARRIO": "OSORIO XII"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004617",
        "NOMBRE_BARRIO": "TINTALITO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004618",
        "NOMBRE_BARRIO": "DINTALITO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004619",
        "NOMBRE_BARRIO": "CHUCUA DE LA VACA III"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004620",
        "NOMBRE_BARRIO": "CHUCUA DE LA VACA II"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004621",
        "NOMBRE_BARRIO": "CHUCUA DE LA VACA I"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004626",
        "NOMBRE_BARRIO": "LAS ACACIAS"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "004627",
        "NOMBRE_BARRIO": "GALAN"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006501",
        "NOMBRE_BARRIO": "COOPERATIVA DE SUB-OFICIALES"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006502",
        "NOMBRE_BARRIO": "MARSELLA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006503",
        "NOMBRE_BARRIO": "VISION DE ORIENTE"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006504",
        "NOMBRE_BARRIO": "BAVARIA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006505",
        "NOMBRE_BARRIO": "PIO XII"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006506",
        "NOMBRE_BARRIO": "CASTILLA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006507",
        "NOMBRE_BARRIO": "NUEVO TECHO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006508",
        "NOMBRE_BARRIO": "LAS DOS AVENIDAS"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006509",
        "NOMBRE_BARRIO": "LUSITANIA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006510",
        "NOMBRE_BARRIO": "VILLA ALSACIA II"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006511",
        "NOMBRE_BARRIO": "LA PAMPA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006512",
        "NOMBRE_BARRIO": "VILLA ALSACIA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006514",
        "NOMBRE_BARRIO": "VERGEL OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006515",
        "NOMBRE_BARRIO": "VALLADOLID"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006516",
        "NOMBRE_BARRIO": "TINTALA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006517",
        "NOMBRE_BARRIO": "MARIA PAZ"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006518",
        "NOMBRE_BARRIO": "OSORIO III"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006524",
        "NOMBRE_BARRIO": "LA MAGDALENA"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006529",
        "NOMBRE_BARRIO": "EL VERGEL ORIENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006531",
        "NOMBRE_BARRIO": "CIUDAD TECHO II"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006533",
        "NOMBRE_BARRIO": "EL TINTAL III"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "006534",
        "NOMBRE_BARRIO": "EL TINTAL IV"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "105105",
        "NOMBRE_BARRIO": "VEREDA EL TINTAL RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "105203",
        "NOMBRE_BARRIO": "OSORIO II"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "205101",
        "NOMBRE_BARRIO": "VEREDA EL TINTAL URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "205209",
        "NOMBRE_BARRIO": "EL JAZMIN"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "205221",
        "NOMBRE_BARRIO": "GALAN RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "8",
        "NOMBRE_LOCALIDAD": "KENNEDY",
        "CODIGO_BARRIO": "205225",
        "NOMBRE_BARRIO": "LAS ACACIAS RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "005624",
        "NOMBRE_BARRIO": "AEROPUERTO EL DORADO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "005672",
        "NOMBRE_BARRIO": "LAS NAVETAS"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "005673",
        "NOMBRE_BARRIO": "PUEBLO VIEJO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006301",
        "NOMBRE_BARRIO": "SANTA CECILIA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006302",
        "NOMBRE_BARRIO": "CAPELLANIA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006303",
        "NOMBRE_BARRIO": "LA ESPERANZA NORTE"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006306",
        "NOMBRE_BARRIO": "GRANJAS DE TECHO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006307",
        "NOMBRE_BARRIO": "MONTEVIDEO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006308",
        "NOMBRE_BARRIO": "FRANCO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006311",
        "NOMBRE_BARRIO": "MODELIA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006312",
        "NOMBRE_BARRIO": "MODELIA OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006313",
        "NOMBRE_BARRIO": "SALITRE OCCIDENTAL"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006315",
        "NOMBRE_BARRIO": "LA ESPERANZA SUR"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006316",
        "NOMBRE_BARRIO": "BOSQUE DE MODELIA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006319",
        "NOMBRE_BARRIO": "TERMINAL DE TRANSPORTES"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006320",
        "NOMBRE_BARRIO": "CIUDAD HAYUELOS"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006401",
        "NOMBRE_BARRIO": "VERSALLES FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006402",
        "NOMBRE_BARRIO": "LA CABANA FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006403",
        "NOMBRE_BARRIO": "SAN JOSE DE FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006404",
        "NOMBRE_BARRIO": "PUERTA DE TEJA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006405",
        "NOMBRE_BARRIO": "FERROCAJA FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006406",
        "NOMBRE_BARRIO": "VILLEMAR"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006407",
        "NOMBRE_BARRIO": "GUADUAL FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006408",
        "NOMBRE_BARRIO": "EL CARMEN FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006409",
        "NOMBRE_BARRIO": "BELEN FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006410",
        "NOMBRE_BARRIO": "CENTRO FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006411",
        "NOMBRE_BARRIO": "CHARCO URBANO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006413",
        "NOMBRE_BARRIO": "SAN PABLO JERICO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006414",
        "NOMBRE_BARRIO": "BRISAS ALDEA FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006415",
        "NOMBRE_BARRIO": "LA LAGUNA FONTIBON"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006416",
        "NOMBRE_BARRIO": "ATAHUALPA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006417",
        "NOMBRE_BARRIO": "VILLA CARMENZA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006418",
        "NOMBRE_BARRIO": "PUENTE GRANDE"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006419",
        "NOMBRE_BARRIO": "EL REFUGIO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006420",
        "NOMBRE_BARRIO": "LA GIRALDA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006423",
        "NOMBRE_BARRIO": "EL TINTAL CENTRAL"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006519",
        "NOMBRE_BARRIO": "MORAVIA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006520",
        "NOMBRE_BARRIO": "ZONA FRANCA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006521",
        "NOMBRE_BARRIO": "SABANA GRANDE"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006522",
        "NOMBRE_BARRIO": "EL TINTAL II"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006523",
        "NOMBRE_BARRIO": "SAN PEDRO DE LOS ROBLES"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006525",
        "NOMBRE_BARRIO": "EL CHANCO I"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006527",
        "NOMBRE_BARRIO": "INTERINDUSTRIAL"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006528",
        "NOMBRE_BARRIO": "EL VERGEL"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "006532",
        "NOMBRE_BARRIO": "KASANDRA"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "105103",
        "NOMBRE_BARRIO": "SAN PEDRO"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "105104",
        "NOMBRE_BARRIO": "EL TINTAL"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "105401",
        "NOMBRE_BARRIO": "EL CHANCO RURAL II"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "105402",
        "NOMBRE_BARRIO": "CHARCO RURAL"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "105403",
        "NOMBRE_BARRIO": "EL CHANCO RURAL III"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "205102",
        "NOMBRE_BARRIO": "VEREDA EL TINTAL"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "205401",
        "NOMBRE_BARRIO": "EL CHANCO II"
    },
    {
        "CODIGO_LOCALIDAD": "9",
        "NOMBRE_LOCALIDAD": "FONTIBON",
        "CODIGO_BARRIO": "205402",
        "NOMBRE_BARRIO": "EL CHARCO"
    }
]