import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingRequestComponent } from './loading-request.component';

describe('LoadingRequestComponent', () => {
  let component: LoadingRequestComponent;
  let fixture: ComponentFixture<LoadingRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
