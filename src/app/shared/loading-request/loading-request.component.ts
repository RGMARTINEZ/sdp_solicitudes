import { Component, OnInit } from '@angular/core';
import { ConfiguracionService } from '../../integrador/services/configuracion.service';


@Component({
  selector: 'sc-loading-request',
  templateUrl: './loading-request.component.html',
  styles: []
})
export class LoadingRequestComponent implements OnInit {

  constructor(public settings: ConfiguracionService) { }

  ngOnInit() {
  }

  public stopLoading(){
    this.settings.showLoadingRequest(false);
  }

}
