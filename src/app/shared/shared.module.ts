import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';

import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';
import { ToggleFullscreenDirective } from './fullscreen/toggle-fullscreen.directive';
import {CardRefreshDirective} from './card/card-refresh.directive';
import {CardToggleDirective} from './card/card-toggle.directive';
import { CardComponent } from './card/card.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ParentRemoveDirective} from './elements/parent-remove.directive';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ModalAnimationComponent} from './modal-animation/modal-animation.component';
import {ModalBasicComponent} from './modal-basic/modal-basic.component';
import {SimpleNotificationsModule} from 'angular2-notifications';
import {TagInputModule} from 'ngx-chips';
import {AnimatorModule} from 'css-animator';
import {SelectModule} from 'ng-select';
import {SelectOptionService} from './elements/select-option.service';
//import {FormWizardModule} from 'angular2-wizard';
import {DataFilterPipe} from './elements/data-filter.pipe';
import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';
import {ClickOutsideModule} from 'ng-click-outside';
import {SpinnerComponent} from './spinner/spinner.component';
import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {NotificationsService} from 'angular2-notifications';
import {ToastyModule} from 'ng2-toasty';
import { OrderModule } from 'ngx-order-pipe'; // <- import OrderModule
import { CurrencyMaskModule } from "ng2-currency-mask";
import { MaterialModule } from "./material/material-module";
import { FlexLayoutModule } from '@angular/flex-layout';
import { FileUploadModule } from 'ng2-file-upload';





const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      NgbModule.forRoot(),
      SimpleNotificationsModule.forRoot(),
      TagInputModule,
      AnimatorModule,
      SelectModule,
      ScrollToModule.forRoot(),
      ClickOutsideModule,
      PerfectScrollbarModule,
      ToastyModule.forRoot(),
      OrderModule,
      CurrencyMaskModule,
      MaterialModule,
      FlexLayoutModule,
      FileUploadModule
  ],
  declarations: [
      AccordionAnchorDirective,
      AccordionLinkDirective,
      AccordionDirective,
      ToggleFullscreenDirective,
      CardRefreshDirective,
      CardToggleDirective,
      ParentRemoveDirective,
      CardComponent,
      SpinnerComponent,
      ModalAnimationComponent,
      ModalBasicComponent,
      DataFilterPipe
  ],
  exports: [
      AccordionAnchorDirective,
      AccordionLinkDirective,
      AccordionDirective,
      ToggleFullscreenDirective,
      CardRefreshDirective,
      CardToggleDirective,
      ParentRemoveDirective,
      CardComponent,
      SpinnerComponent,
      NgbModule,
      FormsModule,
      ReactiveFormsModule,
      ModalBasicComponent,
      ModalAnimationComponent,
      SimpleNotificationsModule,
      TagInputModule,
      AnimatorModule,
      SelectModule,
      DataFilterPipe,
      ScrollToModule,
      ClickOutsideModule,
      PerfectScrollbarModule,
      ToastyModule,
      OrderModule,
      CurrencyMaskModule,
      MaterialModule,
      FileUploadModule

  ],
  providers: [
      SelectOptionService,
      NotificationsService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]
})
export class SharedModule { }
