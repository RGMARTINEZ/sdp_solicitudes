import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './layouts/admin/admin.component';

const routes: Routes = [
  {
    path: '',
    redirectTo : 'registro-solicitudes',
    pathMatch : 'full'
  },
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'registro-solicitudes',
        pathMatch: 'full'
      },
      {
        path: 'registro-solicitudes',
        loadChildren: './registro/registro.module#RegistroModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'registro-solicitudes'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
