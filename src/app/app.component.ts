import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import { ConfiguracionService } from '../app/integrador/services/configuracion.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  showLoading = false;
  showLoadingRequest = false;
  subShowLoading = null;
  subShowLoadingRequest = null;

  constructor(private router: Router,
    public settings: ConfiguracionService) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });

    this.subShowLoading = this.settings.subShowLoading.subscribe(flat => {
      this.showLoading = flat;
    });

    this.subShowLoadingRequest = this.settings.subShowLoadingRequest.subscribe(flat2 => {
      this.showLoadingRequest = flat2;
    });


  }
}
