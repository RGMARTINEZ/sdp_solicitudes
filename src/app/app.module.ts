import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {IntegradorModule} from './integrador/integrador.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './layouts/admin/admin.component';
import { TitleComponent } from './layouts/admin/title/title.component';
import { BreadcrumbsComponent } from './layouts/admin/breadcrumbs/breadcrumbs.component';
import {SharedModule} from './shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './integrador/services/token-interceptor';
import { AuthService } from './integrador/services/auth.service';
import { HttpService } from './integrador/services/http.service';
import { TokenAdminService } from './integrador/services/token-admin.service';
import { LoadingPageComponent } from '../app/shared/loading-page/loading-page.component';
import { LoadingRequestComponent } from '../app/shared/loading-request/loading-request.component';

import { LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    TitleComponent,
    BreadcrumbsComponent,
    LoadingPageComponent,
    LoadingRequestComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    IntegradorModule
  ],
  providers: [
    AuthService,
    HttpService,
/*
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }, */ 
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
