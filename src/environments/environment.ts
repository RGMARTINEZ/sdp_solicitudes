// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //END_POINT_SSO : 'http://localhost:5000/',              // Desarrollo Local SDP
  //END_POINT_SSO : 'http://localhost:5100/api/v1/',              // Desarrollo Local DOCKER

  //END_POINT_SSO : 'https://sisbensolbackpru.sdp.gov.co/', // Pruebas SDP
  END_POINT_SSO : 'https://sisbensolback.sdp.gov.co/', // Produccion SDP


  API_KEY: '6868ab95fb3be68cb43d118a732ccf493295efd8',
  API_SECRET: '717536b3a013916fad09ac289d69aae41adf1b6c'

};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.





